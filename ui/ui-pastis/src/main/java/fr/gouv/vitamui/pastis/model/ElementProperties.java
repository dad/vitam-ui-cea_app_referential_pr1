/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.model;

import fr.gouv.vitamui.pastis.util.RNGConstants;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo Pimenta <pimenta@cines.fr>
 */
public class ElementProperties implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = -5093112183496503253L;


	public ElementProperties() {
        super();
    }

    String name;
    String type;
    String cardinality;
    String groupOrChoice;
    String valueOrData;
    String dataType;
    String value;
    String documentation;

    int level;
    Long id;
    Long parentId;

    @JsonIgnore
    ElementProperties parent;

    List<ElementProperties> choices = new ArrayList<ElementProperties>();

    List<ElementProperties> children = new ArrayList<ElementProperties>();

    public List<ElementProperties> getChildren() {
        return this.children;
    }

    public void setChildren(List<ElementProperties> children) {
        this.children = children;
    }

    public List<ElementProperties> getChoices() {
		return choices;
	}

	public void setChoices(List<ElementProperties> choices) {
		this.choices = choices;
	}

	public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValueOrData() {
        return this.valueOrData;
    }

    public void setValueOrData(String dataType) {
        this.valueOrData = dataType;
    }

    public String getCardinality() {
        return this.cardinality;
    }

    public void setCardinality(String cardinality) {
        if(null != RNGConstants.CardinalityMap.get(cardinality)) {
            this.cardinality = RNGConstants.CardinalityMap.get(cardinality);
        }else {
            this.cardinality = cardinality;
        }
    }

    public String getGroupOrChoice() {
		return groupOrChoice;
	}

	public void setGroupOrChoice(String groupOrChoice) {
		if(null != RNGConstants.GroupOrChoiceMap.get(groupOrChoice)) {
            this.groupOrChoice = RNGConstants.GroupOrChoiceMap.get(groupOrChoice);
        }else {
            this.groupOrChoice = groupOrChoice;
        }
	}

	public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDataType() {
        return this.dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }


    public String getDocumentation() {
       return this.documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @JsonIgnore
    public ElementProperties getParent() {
        return this.parent;
    }

    @JsonIgnore
    public void setParent(ElementProperties parent) {
        this.parent = parent;
    }

    public void init() {
        this.setName("");
        this.setCardinality("");
        this.setValueOrData("");
        this.setGroupOrChoice("");
        this.setValue("");
        this.setType("");
        this.setDataType("");

    }
    public void initTree(ElementProperties json) {
    	for(ElementProperties child : json.getChildren()) {
    		child.setParent(json);
    		initTree(child);
    	}
   }
}
