/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.model.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="rng:attribute")
public class AttributeXML extends BaliseXML{

    ValueXML valueXML;

    @XmlElement (name="rng:value")
    public ValueXML getValueXML() {
        return valueXML;
    }

    public void setValueXML(ValueXML valueXML) {
        this.valueXML = valueXML;
    }

}
