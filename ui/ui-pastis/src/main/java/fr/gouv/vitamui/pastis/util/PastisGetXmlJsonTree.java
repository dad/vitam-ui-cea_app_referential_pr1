package fr.gouv.vitamui.pastis.util;

import com.fasterxml.jackson.xml.XmlMapper;
import fr.gouv.vitamui.pastis.model.ElementProperties;
import fr.gouv.vitamui.pastis.model.ElementRNG;
import fr.gouv.vitamui.pastis.model.jaxb.*;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PastisGetXmlJsonTree {

    //ElementRNG elementRNGRoot;
    public String jsonParsed = "";
    private static Logger LOGGER = LoggerFactory.getLogger(PastisGetXmlJsonTree.class);


    public String getJsonParsedTree(ElementRNG elementRNGRoot) {

        ElementRNG.buildElementPropertiesTree(elementRNGRoot,0, null);
        ElementProperties eparent  = ElementRNG.elementStaticRoot;
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";

        try {
            jsonString = mapper.writeValueAsString(eparent);
        } catch (JsonGenerationException e1) {
            e1.printStackTrace();
        } catch (JsonMappingException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        LOGGER.info("JSON file successfully generated");
        return "["+jsonString+"]";
    }

    // Test an xml to json and a json to xml.
    public String getJsonParsedTreeTest(ElementRNG elementRNGRoot) throws JAXBException, FileNotFoundException {

    	//vWhen the handler is called, the proprities tree (json) is build
    	// using its ElementRNG(elementRngRoot) object.
    	// The elementRngRoot is filled when the xml file is read, by passing
    	// it to the contentHanler of the  Xml reader.
    	// The methods used are the 5 main methods of a DefaultHandler type
    	// See methods bellow
        ElementRNG.buildElementPropertiesTree(elementRNGRoot,0, null);
        ElementProperties eparent  = ElementRNG.elementStaticRoot;


        // The eparentRng is an object of type BalizeXML. It is  built using the
        // object eparent (of type ElementProperties) that, in fact, represent the json
        // prouced during the parser's first call.
        BaliseXML.buildBaliseXMLTree(eparent,0, null);
        BaliseXML eparentRng  = BaliseXML.baliseXMLStatic;


        // Transforms java objects to Xml file (Marshalling)
        JAXBContext contextObj = JAXBContext.newInstance(AttributeXML.class, ElementXML.class, DataXML.class, ValueXML.class, OptionalXML.class, OneOrMoreXML.class,
        		ZeroOrMoreXML.class, AnnotationXML.class, DocumentationXML.class);
        Marshaller marshallerObj = contextObj.createMarshaller();
        marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerObj.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler",
        	    new PastisCustomCharacterEscapeHandler());

        marshallerObj.marshal(eparentRng, new FileOutputStream("generated_test.xml"));


        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(eparent);
        } catch (JsonGenerationException e1) {
            e1.printStackTrace();
        } catch (JsonMappingException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "["+jsonString+"]";
    }


    public String getXmlParsedTree(String jsonString) throws JsonProcessingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectMapper xmlMapper = new XmlMapper();
        JsonNode tree = objectMapper.readTree(jsonString);
        String jsonAsXml = xmlMapper.writeValueAsString(tree);

        return jsonAsXml;
    }


    public void setJsonParsed(String jsonParsed) {
        this.jsonParsed = jsonParsed;
    }



}
