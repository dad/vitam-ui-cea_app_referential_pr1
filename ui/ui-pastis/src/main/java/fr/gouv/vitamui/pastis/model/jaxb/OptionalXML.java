/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.model.jaxb;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rachid Sala <rachid@cines.fr>
 */
@XmlRootElement (name="rng:optional")
public class OptionalXML extends BaliseXML{

}
