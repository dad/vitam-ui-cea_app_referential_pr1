import { IEnvironment } from './IEnvironment';

export const environment: IEnvironment = {
  production: true,
  apiPastisUrl: 'http://10.100.129.51:8888/rest',
  apiOntologyUrl: 'http://ontology.api.adress:8888',
  name: 'prod'
};
