import{IEnvironment} from './IEnvironment'

export const environment: IEnvironment= {
  production: false,
  apiPastisUrl: 'http://vps795748.ovh.net:8080/rest',
  apiOntologyUrl: 'http://vps795748.ovh.net:8080',
  name: 'dev'
};
