import {environment as envProd} from '../../../environments/environment.prod'
import {environment as  envDev} from '../../../environments/environment.dev'
import { isDevMode, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class PastisApiRoute {

    rootApiPath: string;
    apiUrls: {};
    apiPathMap : Map<string,string>;


    constructor() {
        this.rootApiPath = isDevMode() ? envDev.apiPastisUrl : envProd.apiPastisUrl;
        this.apiPathMap = new Map();

        const apiEntryPoints= {
            'createprofile' : '/createprofile',
            'updateprofile' : '/updateprofile',
            'getfile' : '/getFile',
            'profilefromfilePath' :'/createprofilefromfile',
            'getSedaFile' :'/assets/seda.json'

        }
        for(var entryProint in apiEntryPoints) {
            if (apiEntryPoints.hasOwnProperty(entryProint)) {
                console.log(entryProint);
                this.apiPathMap.set(entryProint+'Path',apiEntryPoints[entryProint])
            }
        }


    }



}
