import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule} from '@angular/material/button';
import { MatIconModule, MatProgressSpinnerModule } from '@angular/material';

import { PastisApiService } from './services/api.pastis.service';
import { OntologyApiService } from './services/api.ontology.service';

import { FileService } from './services/file.service';
import { ProfileService } from './services/profile.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PastisSpinnerInterceptor } from '../shared/pastis-spinner/pastis-spinner-interceptor';
import { SharedModule } from '../shared/shared.module';
import { SedaService } from './services/seda.service';
import { RegisterIconsService } from './services/register-icons.service';
import { NotificationService } from './services/notification.service';
import { SettingsService } from './services/settings.service';
import { PopupService } from './services/popup.service';

import {
  NgxUiLoaderModule,
  NgxUiLoaderConfig,
  SPINNER,
  POSITION,
  PB_DIRECTION
} from 'ngx-ui-loader';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "bgsColor": "red",
  "bgsOpacity": 0.5,
  "bgsPosition": "bottom-right",
  "bgsSize": 60,
  "bgsType": "ball-spin-clockwise",
  "blur": 5,
  "delay": 0,
  "fgsColor": "#ffffff",
  "fgsPosition": "center-center",
  "fgsSize": 60,
  "fgsType": "ball-spin-clockwise",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 120,
  "logoUrl": "assets/pastis-logo7.png",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(40,40,40,0.8)",
  "pbColor": "#604379",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": false,
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
};
@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    SharedModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
  ],
  exports: [NgxUiLoaderModule],
  providers: [
    PastisSpinnerInterceptor,
    FileService,
    ProfileService,
    PastisApiService,
    OntologyApiService,
    SedaService,
    RegisterIconsService,
    NotificationService,
    SettingsService,
    PopupService,

    { provide: HTTP_INTERCEPTORS, useClass: PastisSpinnerInterceptor,multi: true}
  ],
  declarations: [],
})
export class CoreModule {

}
