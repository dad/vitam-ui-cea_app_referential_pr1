import { EventEmitter, Injectable, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { SedaData, SedaElementConstants, SedaCardinalityConstants } from 'src/app/file-tree/classes/seda-data';
import { FileNode, TypeConstants, CardinalityConstants } from '../../file-tree/classes/file-node';
import { PastisDialogConfirmComponent } from '../../shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { ProfileService } from './profile.service';
import { PastisDialogData } from 'src/app/shared/pastis-dialog/classes/pastis-dialog-data';
import { SedaService } from './seda.service';
import { FileTreeMetadataService } from 'src/app/file-tree/file-tree-metadata/file-tree-metadata.service';

@Injectable({
  providedIn: 'root'
})

export class FileService  {

  @Output()
  public addNode: EventEmitter<FileNode> = new EventEmitter<FileNode>();
  @Output()
  public removeNode: EventEmitter<FileNode> = new EventEmitter<FileNode>();
  @Output()
  public renderChanges: EventEmitter<boolean> = new EventEmitter<boolean>();

  dataChange = new BehaviorSubject<FileNode[]>([]);
  nodeChange = new BehaviorSubject<FileNode>(null);
  allData = new BehaviorSubject<FileNode[]>([]);

  rootTabName = new BehaviorSubject<string>(null);

  filteredNode = new BehaviorSubject<FileNode>(null);
  tabChildrenRulesChange = new BehaviorSubject<string[][]>([]);


  private params = {};
  parentNodeMap = new Map<FileNode, FileNode>();
  private formatErrors(error: any) {
    return  throwError('An error : '  + error.error);
  }

  constructor(private profileService: ProfileService,  private fileMetadataService: FileTreeMetadataService,
    private dialog: MatDialog, private sedaService: SedaService) { }

  getCurrentFileTree(): Observable<FileNode[]> {
      console.log("On file service : ", this.dataChange.getValue())
      return this.dataChange;
  }

  getFileTreeFromApi(): Observable<FileNode[]>  {
    this.profileService.getProfile().subscribe(data => {
      this.dataChange.next(data);
      });
    return this.dataChange;
  }

  updateFileTree(newData: FileNode[]): Observable<FileNode[]> {
    this.dataChange.next(newData);
    return this.dataChange;
  }

  updateNode(newNode: FileNode, parentNode:FileNode) {
    let parentNodeToChange = this.allData.getValue().find(node=> node.id === newNode.id);
    for(let node of parentNodeToChange.children){
      if (node.id === newNode.id) {
        node = newNode;
     }
    }
  }

  sendNode(node:FileNode) {
    this.nodeChange.next(node);
    console.log("Node on file file service : ", this.nodeChange.getValue());
  }

  openPopup(popData: PastisDialogData){
    const dialogConfirmRef = this.dialog.open(PastisDialogConfirmComponent, {
      width: popData.width,
      height: popData.height,
      data: popData,
      panelClass: 'pastis-popup-modal-box'
    });
    return new Promise((resolve, reject) => {
      dialogConfirmRef.afterClosed().subscribe(data => {
        resolve(data);
        console.log("The confirm dialog was closed with data : ", data);
      }, reject)
    });
  }

  findChild(nodeName:string,node:FileNode):FileNode {
    for (let child of node.children) {
      if (child.name === nodeName){
        return child;
      }
    }
  }

  setTabRootName(rootTabName:string){
    this.rootTabName.next(rootTabName);
  }


  openDialogWithTemplateRef(templateRef) {
    this.dialog.open(templateRef);
  }

  setNewChildrenRules(rules:string[][]){
      this.tabChildrenRulesChange.next(rules);
  }

  /**
   * Get one attribute of the node by its name
   * @param fileNode The concerned node
   * @param attributeName The name of the attribute we want to get
   */
  getAttributeByName(fileNode:FileNode, attributeName:String):FileNode {
    return fileNode.children.find(c=>c.name==attributeName);
  }

  /**
   * Delete all the attributes of the concerned FileNode
   * @param fileNode The concerned FileNode
   */
  deleteAllAttributes(fileNode: FileNode):void {
    fileNode.children = fileNode.children.filter(c=>c.type!==TypeConstants.attribute);
  }

  insertItem(parent: FileNode, elementsToAdd: FileNode[],sedaData:SedaData) {
    if (parent.children && elementsToAdd) {
      let sedaParent = this.sedaService.getSedaNodeLocally(sedaData, parent.name)
      for (let element of elementsToAdd) {
        let newNode = {} as FileNode;
        // Get an index based on last child of the parent
        let sedaChild = this.sedaService.getSedaNodeLocally(sedaData, element.name);
        newNode.name = element.name;
        newNode.id = window.crypto.getRandomValues(new Uint32Array(10))[0];;
        newNode.level = parent.level + 1;
        newNode.type = TypeConstants.element;
        newNode.parentId = parent.id;
        newNode.children = [];
        newNode.value = element.value;
        newNode.documentation = element.documentation;
        // Check if the node to be added has its own cardinality.
        // If not, the new node will take seda's default cardinality
        if (element.cardinality) {
          newNode.cardinality = element.cardinality
        } else {
          newNode.cardinality = Object.values(CardinalityConstants).find(c => c.valueOf() === sedaChild.Cardinality);
        }
        console.log("Parent node name: " + parent.name);
        console.log("New node  : ", newNode);

        parent.children.push(newNode);
        this.parentNodeMap.set(newNode, parent);
        console.log("Set new node name: " + newNode.name);
        console.log("Seda children and file children: ", sedaData.Children, parent.children)
        // Insert all children of complex elements (not attributes) based on SEDA definition
        if (element.type === TypeConstants.element){
          let complexChildrenElements = this.getComplexSedaChildrenAsFileNode(sedaChild);
          if (complexChildrenElements.length){
              this.insertItem(parent,complexChildrenElements,sedaChild);
          }
        }

      }
        //Create a list of names of children names based on seda
        let sedaChildrenName: string[] = [];
        sedaParent.Children.forEach(child => {
          sedaChildrenName.push(child.Name);
        })
        //Order elements according to seda children's name list
        parent.children.sort((a, b) => {
            return sedaChildrenName.indexOf(a.name) - sedaChildrenName.indexOf(b.name)
        })
        console.log("After filter : ", parent.children)
        this.updateItem(parent);
        //this.sendNodeMetadata(parent);
        console.log("New fileNode data is : %o", this.dataChange.getValue())

    } else {
      console.log('No More Nodes can be inserted : No node was selected or node name is invalid');
    }
  }

    /** Update an item Tree node */
    updateItem(node: FileNode) {
      this.dataChange.next(node[0]);
      console.log("Node updated to : ", this.dataChange.getValue())

    }

    removeItem(nodesToBeDeleted: FileNode[], root: FileNode) {
      if (nodesToBeDeleted.length) {
        for (let node of nodesToBeDeleted) {
          let nodeToBeDeleted = this.getFileNodeByName(root,node.name);
          //Check if node exists in the file tree
          if (nodeToBeDeleted) {
            const parentNode = this.findParent(nodeToBeDeleted.parentId, root);
            console.log("On removeItem with node : ", nodeToBeDeleted, "and parent : ", parentNode);
            const index = parentNode.children.indexOf(nodeToBeDeleted);
            if (index !== -1) {
              parentNode.children.splice(index, 1);
              this.parentNodeMap.delete(nodeToBeDeleted);
            }
            console.log("Deleted node : ", nodeToBeDeleted, "and his parent : ", parentNode);
          }
        }
      }
      console.log("No nodes will be deleted")
    }

     /** Find a parent tree node */
  findParent(id: number, node: FileNode): FileNode {
    console.log("On findParent with parent node id : ", id , " and node : ", node);
    if (node && node.id === id) {
      return node;
    } else {
      //console.log('ELSE ' + JSON.stringify(node.children));
      if (node.children && id) {
        for (let element = 0; node.children.length; element++) {
          //console.log('Recursive ' + JSON.stringify(node.children[element].children));
          //console.error("Node here : ", node.children[element], element)
          if (element && node.children && node.children.length > 0 && node.children[element].children.length > 0) {
            return this.findParent(id, node.children[element]);
          } else {
            continue;
          }
        }
      }
    }
  }

    sendNodeMetadata(node: FileNode) {
      let rulesFromService = this.tabChildrenRulesChange.getValue()
      let tabChildrenToInclude = rulesFromService[0];
      let tabChildrenToExclude = rulesFromService[1];
      let sedaParent = this.sedaService.selectedSedaNodeParent.getValue()
      let theNode = this.sedaService.getSedaNodeLocally(sedaParent, node.name);
      console.log("Node clicked : ", node, "...with tab rules from service : ", rulesFromService);

      if (theNode) {
        console.log("The found node on filetree : ", theNode)
        this.sedaService.selectedSedaNode.next(theNode);
        this.dataChange.next(node[0]);
        this.sendNode(node);
        let dataTable = this.fileMetadataService.fillDataTable(theNode, node, tabChildrenToInclude, tabChildrenToExclude);
        console.log("Data revtried on click : ", dataTable);
        console.log("Node seda %s in filetree is ready to be edited with seda data %o", node.name, this.sedaService.selectedSedaNode.getValue());
        this.fileMetadataService.dataSource.next(dataTable);
      }
    }

    getFileNodeByName(fileTree:FileNode, nodeNameToFind:string) {
      if (fileTree){
      if (fileTree.name === nodeNameToFind) {
        return fileTree;
      }
      for (const child of fileTree.children) {
        const res = this.getFileNodeByName(child, nodeNameToFind);
        if (res) {
          return res;
        }
      }
    }
    }

    getFileNodeById(fileTree:FileNode, nodeIdToFind:number) {
      if (fileTree){
      if (fileTree.id === nodeIdToFind) {
        return fileTree;
      }
      for (const child of fileTree.children) {
        const res = this.getFileNodeById(child, nodeIdToFind);
        if (res) {
          return res;
        }
      }
    }
    }

    getFileNodeLocally(currentNode:FileNode, nameNode:string):FileNode {
      //console.log("Node on this.findSedaNode : %o", currentNode)
      if (currentNode){
      let i: number, currentChild: FileNode;
      if (nameNode == currentNode.name ) {
        return currentNode;
      } else {
        // Use a for loop instead of forEach to avoid nested functions
        // Otherwise "return" will not work properly
        if (currentNode.children) {
          for (i = 0; i < currentNode.children.length; i += 1) {
            currentChild = currentNode.children[i];
            // Search in the current child
            let result = this.getFileNodeLocally(currentChild,nameNode);
            // Return the result if the node has been found
            if (result) {
              return result;
            }
          }
        } else {
            // The node has not been found and we have no more options
            console.log("No SEDA nodes could be found for ", nameNode);
            return;
        }
      }
    }
  }

   getComplexSedaChildrenAsFileNode(sedaElement:SedaData):FileNode[] {
    // Insert all children of complex elements based on SEDA definition
    if (sedaElement.Element === SedaElementConstants.complex &&
      sedaElement.Children.length > 0) {
          let fileNodeComplexChildren : FileNode[] = [];
          sedaElement.Children.forEach(child=> {
                if (child.Cardinality === SedaCardinalityConstants.one ||
                    child.Cardinality === SedaCardinalityConstants.oreOrMore) {
                      let aFileNode : FileNode = {} as FileNode;
                      aFileNode.name = child.Name;
                      aFileNode.cardinality = child.Cardinality;
                      aFileNode.children = [];
                      aFileNode.type = TypeConstants[child.Type];
                      fileNodeComplexChildren.push(aFileNode);
                }
              })
              return fileNodeComplexChildren
            }

    }
  }
