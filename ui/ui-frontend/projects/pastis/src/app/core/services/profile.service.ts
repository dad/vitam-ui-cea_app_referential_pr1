import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FileNode } from '../../file-tree/classes/file-node';
import { PastisApiService } from './api.pastis.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private getProfileUrl = '/createprofile';
  private uploadProfileUrl = '/createprofilefromfile';
  private getFileUrl = '/getfile';
  private updateFileUrl = '/updateprofile';

  constructor(private apiService: PastisApiService) { }

  getProfile(): Observable<FileNode[]> {
    return this.apiService.get<FileNode[]>(this.getProfileUrl);
  }

  // Upload a RNG file to the server
  // Response : a JSON object
  uploadProfile(profile: FormData): Observable<FileNode[]> {
    console.log("On upload profile");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data',
      })
    };
    return this.apiService.post(this.uploadProfileUrl, profile);
  }

  // Get the base rng profile
  getFile(): Observable<Blob> {
    const options = {responseType: 'blob'};
    return this.apiService.get(this.getFileUrl, options);
  }
  // Get the modified tree as an RNG file
  updateFile(file: FileNode[]): Observable<Blob> {

    /*let headers = new HttpHeaders({
      'Content-type': 'application/json',
      responseType: 'blob'
    }); */
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      }),
      responseType: 'blob'
    };
    return this.apiService.post(this.updateFileUrl, file[0], httpOptions);
  }
}
