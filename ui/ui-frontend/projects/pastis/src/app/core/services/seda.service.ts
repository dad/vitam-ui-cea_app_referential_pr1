import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable, of } from 'rxjs';
import { CardinalityConstants, FileNode } from '../../file-tree/classes/file-node';
import { SedaData, SedaCardinalityConstants } from '../../file-tree/classes/seda-data';
import { CardinalityValues } from '../classes/models';
import { PastisApiService } from './api.pastis.service';

@Injectable({
  providedIn: 'root'
})
export class SedaService {

  private getSedaUrl = './assets/seda.json';

  selectedSedaNode = new BehaviorSubject<SedaData>(null);
  selectedSedaNodeParent = new BehaviorSubject<SedaData>(null);

  constructor(private pastisAPI : PastisApiService) {}

  getSedaRules(): Observable<SedaData>{
      return this.pastisAPI.getLocally<SedaData>(this.getSedaUrl);
  }

  getSedaNode(currentNode:SedaData, nameNode:string):Observable<SedaData> {
  if (currentNode && nameNode) {
    let i: number, currentChild: SedaData, result: boolean;
    if (nameNode == currentNode.Name ) {
     return of(currentNode);
    } else {
      // Use a for loop instead of forEach to avoid nested functions
      // Otherwise "return" will not work properly
      if (currentNode.Children) {
        for (i = 0; i < currentNode.Children.length; i += 1) {
          currentChild = currentNode.Children[i];
          // Search in the current child
          let result = this.getSedaNode(currentChild,nameNode);
          // Return the result if the node has been found
          if (result) {
            return result;
          }
        }
      }
     // The node has not been found and we have no more options
     return;
   }
  }
  }

  getSedaNodeLocally(currentNode:SedaData, nameNode:string):SedaData {
    //console.log("Node on this.findSedaNode : %o", currentNode)
    let i: number, currentChild: SedaData, result: boolean;
    if (nameNode == currentNode.Name ) {
      return currentNode;
    } else {
      // Use a for loop instead of forEach to avoid nested functions
      // Otherwise "return" will not work properly
      if (currentNode.Children) {
        for (i = 0; i < currentNode.Children.length; i += 1) {
          currentChild = currentNode.Children[i];
          // Search in the current child
          let result = this.getSedaNodeLocally(currentChild,nameNode);
          // Return the result if the node has been found
          if (result) {
            return result;
          }
        }
      } else {
          // The node has not been found and we have no more options
          console.log("No SEDA nodes could be found for ", nameNode);
          return;
      }
    }
  }

  //Get the seda node based on collection name and a node name.
  // Since the SEDA 2.1 model does not contain unique names,
  // the function will search the whole file and return a single metadata based on
  // a node name and a collection name;
  getSedaNodeCollection(sedaNode:SedaData, nodeName:string, collectionName:string):SedaData {
    if (sedaNode){
    if (sedaNode.Collection === collectionName && sedaNode.Name === nodeName) {
      return sedaNode;
    }
    for (const child of sedaNode.Children) {
      const nodeFound = this.getSedaNodeCollection(child, nodeName,collectionName);
      if (nodeFound) {
        return nodeFound;
      }
    }
  }

  }

  // For all correspondent values beetween seda and tree elements,
  // return a SedaData array of elements that does not have
  // an optional (0-1) or an obligatory (1) cardinality.
  // If an element have an 'n' cardinality (e.g. 0-N), the element will
  // aways be included in the list
  findSelectableElementList(sedaNode:SedaData, fileNode:FileNode): SedaData[] {
    let fileNodesNames = fileNode.children.map(e=>e.name);
    let allowedSelectableList = sedaNode.Children.filter(x => (!fileNodesNames.includes(x.Name) &&
                                                      x.Cardinality !== CardinalityConstants.Obligatoire.valueOf())
                                                      ||
                                                      (fileNodesNames.includes(x.Name) &&
                                                      (x.Cardinality === CardinalityConstants["Zero or More"].valueOf())
                                                      ))
    return allowedSelectableList;
  }

  findCardinalityName(clickedNode:FileNode, cardlinalityValues: CardinalityValues[]):string{
    if(!clickedNode.cardinality){
      return "1"
    } else {
        return cardlinalityValues.find(c=>c.value == clickedNode.cardinality).value
    }
  }

  /**
   * Returns the list of all the attributes defined for the node
   * @param sedaNode the seda node we want to query
   */
  getAttributes(sedaNode:SedaData):SedaData[] {
    if (!sedaNode) return;
    return sedaNode.Children.filter(children=>children.Element=="Attribute");
  }

  isSedaNodeObligatory(nodeName:string,sedaParent:SedaData):boolean{
    if (sedaParent){
      for (let child of sedaParent.Children) {
        if (child.Name === nodeName){
          return child.Cardinality.startsWith('1') ? true : false;
        }
      }
    }
  }

  checkSedaElementType(nodeName:string, sedaNode:SedaData):string{
    if (sedaNode.Name === nodeName) return sedaNode.Element;

    let node = sedaNode.Children.find(c=>c.Name==nodeName);
    if (node) {
      return node.Element
    }
    //return false;
  }
  findSedaChildByName(nodeName: string, sedaNode:SedaData): SedaData{
    const childFound = sedaNode.Children.find(c=>c.Name==nodeName);
    return childFound ? childFound : null;
  }

  // Returns a list of cardinalities of a given a fileNode's children
  // If an attributte child doesn't not have a cardinality
  // then the seda child's cardinality will be added by default;
  getCardinalitiesOfSedaChildrenAttributes(fileNode:FileNode,sedaNode:SedaData):string[]{
    let cardinalities : string[] = []
    for (let fileChild of fileNode.children){
      for (let sedaChild of sedaNode.Children){
        if (fileChild.name === sedaChild.Name){
          fileChild.cardinality ?
          cardinalities.push(fileChild.cardinality) :
          cardinalities.push(sedaChild.Cardinality);
        }
      }
    }
    return cardinalities;
  }
}


