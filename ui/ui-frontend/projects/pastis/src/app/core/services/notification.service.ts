
import {Injectable} from '@angular/core'
import { ToastrService } from 'ngx-toastr';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
  MatSnackBarRef
} from '@angular/material';

@Injectable({
  providedIn:'root'
})
export class NotificationService {

   constructor(private toasterService:ToastrService, private snackBar: MatSnackBar){}

  // Toaster
  showSuccess(message:string) {
    this.toasterService.success(message);
  }

  showInfo(message:string) {
    this.toasterService.info(message);
  }

  showError(message:string) {
    this.toasterService.error(message);
  }

  showWarning(message:string) {
    this.toasterService.warning(message);
  }

  //SnackBar

  openSnackBar(message, action, duration) {
    let snackBarConfig = new MatSnackBarConfig();
    let snackBarRef: MatSnackBarRef<any>;
    let horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    let verticalPosition: MatSnackBarVerticalPosition = 'bottom';
    let snackBarAutoHide = '1500';
    snackBarConfig = new MatSnackBarConfig();
    snackBarConfig.horizontalPosition = horizontalPosition;
    snackBarConfig.verticalPosition = verticalPosition;
    snackBarConfig.duration = parseInt(snackBarAutoHide, 0);
    snackBarConfig.panelClass = 'pastis-notifier-bg';
    snackBarConfig.duration = duration * 1000;
    this.snackBar.open(message, action, snackBarConfig)
}



}
