import { TestBed } from '@angular/core/testing';

import { PastisApiService } from './api.pastis.service';

describe('ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PastisApiService = TestBed.get(PastisApiService);
    expect(service).toBeTruthy();
  });
});
