import { Injectable } from '@angular/core';
import { PastisApiService } from './api.pastis.service';
import { Observable} from 'rxjs';
import { SedaData } from '../../file-tree/classes/seda-data';
import { PastisSettings } from '../classes/pastis-settings'
@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private settingsDataUrl = './assets/route-messages.json';

  constructor(private pastisAPI : PastisApiService) {}

  getSettings(): Observable<PastisSettings>{

      return this.pastisAPI.getLocally(this.settingsDataUrl);
  }

}
