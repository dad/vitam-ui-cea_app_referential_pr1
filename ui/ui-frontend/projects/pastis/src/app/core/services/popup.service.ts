import { Injectable, EventEmitter } from '@angular/core';
import { PastisDialogData } from 'src/app/shared/pastis-dialog/classes/pastis-dialog-data';
import { MatDialog } from '@angular/material';
import { PastisDialogConfirmComponent } from 'src/app/shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  popUpDataBeforeClose = new BehaviorSubject<any>(null);
  btnYesShoudBeDisabled= new BehaviorSubject<boolean>(false);


  constructor(private dialog: MatDialog) { }

  getPopUpDataOnClose(){
    return this.popUpDataBeforeClose;
  }
  setPopUpDataOnClose(incomingData:any){
    this.popUpDataBeforeClose.next(incomingData);
  }
  disableYesButton(condition:boolean){
    condition ? this.btnYesShoudBeDisabled.next(true) : this.btnYesShoudBeDisabled.next(false);
  }


}
