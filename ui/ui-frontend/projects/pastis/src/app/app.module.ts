// Angular modules
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';

//Pastis modules
import { CoreModule } from './core/core.module';
import { FileTreeModule } from './file-tree/file-tree.module';
import { UserActionsModule } from './user-actions/user-actions.module';
import { SharedModule } from './shared/shared.module';
import { ProfileModule} from './profile/profile.module'
import { VitamUICommonModule, WINDOW_LOCATION } from 'ui-frontend-common';

//Material modules
import { MatProgressSpinnerModule,
  MatGridListModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule,
  MatDialogModule
} from '@angular/material';

//Routing Modules and components
import { AppRoutingModule,routingComponents } from './app-routing.module';


// Components
import { AppComponent } from './app.component';
import { PastisDialogConfirmComponent } from './shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { UserActionAddMetadataComponent } from './user-actions/user-action-add-metadata/user-action-add-metadata.component';
import { UserActionRemoveMetadataComponent } from './user-actions/user-action-remove-metadata/user-action-remove-metadata.component';
import { PortalModule  } from '@angular/cdk/portal';
import { HomeComponent } from './home/home.component';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    PastisDialogConfirmComponent,
    UserActionAddMetadataComponent,
    UserActionRemoveMetadataComponent,
    routingComponents,
  ],
  entryComponents:[PastisDialogConfirmComponent,UserActionAddMetadataComponent,UserActionRemoveMetadataComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTabsModule,
    MatDialogModule,
    PortalModule,
    UserActionsModule,
    CoreModule,
    FileTreeModule,
    SharedModule,
    AppRoutingModule,
    VitamUICommonModule,
    ProfileModule
  ],
  exports:[
      HttpClientModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MatProgressSpinnerModule,
      MatGridListModule,
      MatToolbarModule,
      MatSidenavModule,
      MatTabsModule,
      UserActionsModule,
      CoreModule,
      FileTreeModule,
      SharedModule
  ],
    providers: [
    Title,
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: WINDOW_LOCATION, useValue: window.location }
    ],
  bootstrap: [AppComponent],

})
export class AppModule { }
