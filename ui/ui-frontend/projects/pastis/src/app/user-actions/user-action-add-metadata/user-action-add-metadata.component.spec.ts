import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActionAddMetadataComponent } from './user-action-add-metadata.component';

describe('UserActionAddMetadataComponent', () => {
  let component: UserActionAddMetadataComponent;
  let fixture: ComponentFixture<UserActionAddMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActionAddMetadataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActionAddMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
