import { Component, OnInit, TemplateRef, Pipe, PipeTransform } from '@angular/core';
import { SedaData, SedaElementConstants, SedaCardinalityConstants } from 'src/app/file-tree/classes/seda-data';
import { FileNode } from 'src/app/file-tree/classes/file-node';
import { FileService } from 'src/app/core/services/file.service';
import { SedaService } from 'src/app/core/services/seda.service';
import { MatDialogRef } from '@angular/material';
import { PastisDialogConfirmComponent } from 'src/app/shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { PastisDialogData } from 'src/app/shared/pastis-dialog/classes/pastis-dialog-data';
import { PopupService } from 'src/app/core/services/popup.service';

@Component({
  selector: 'pastis-user-action-add-metadata',
  templateUrl: './user-action-add-metadata.component.html',
  styleUrls: ['./user-action-add-metadata.component.scss']
})
export class UserActionAddMetadataComponent implements OnInit {

  btnIsDisabled: boolean;

  sedaData:SedaData;
  allowedChildren : SedaData[];
  allowedChildrenNames: string[];
  filterName:string;
  namesFiltered = [];
  sedaNodeFound: SedaData;
  selectedSedaNode:SedaData;
  addedItems:string[] = [];
  dialogData: PastisDialogData;

  atLeastOneIsSelected:boolean;
  customTemplate:TemplateRef<any>
  fileNode: FileNode;

  constructor(public dialogRef: MatDialogRef<PastisDialogConfirmComponent>,
    private fileService:FileService, private sedaService:SedaService,
    private popUpService: PopupService) { }

  ngOnInit() {
    this.fileService.nodeChange.subscribe(fileNode=>{this.fileNode = fileNode})
    setTimeout(() => {
      this.sedaService.getSedaRules().subscribe(data => {
        this.sedaData = data[0];
        this.sedaService.getSedaNode(this.sedaData, this.fileNode.name).subscribe(data=>{
          this.sedaNodeFound = data;
          this.allowedChildren = this.sedaService.findSelectableElementList(this.sedaNodeFound,this.fileNode)
                                                  .filter(e=>e.Element !== SedaElementConstants.attribute);
          console.error("On popup init : ", this.allowedChildren,this.fileNode)
          this.allowedChildrenNames = this.allowedChildren.map(e=>e.Name);
        })
      })
    }, 250);
    // Subscribe observer to button status and
    // set the inital state of the ok button to disabled
    this.popUpService.btnYesShoudBeDisabled.subscribe(status=>{
      this.btnIsDisabled = status;
    })
  }

  selectSedaElement(selectedElements:string[]) {
    if (selectedElements.length) {
    this.sedaService.getSedaNode(this.sedaData,selectedElements[0]).subscribe(data=>{
      this.selectedSedaNode = data;
    });
    }
  }

  isElementSelected(elementName){
    if (this.addedItems){
      return this.addedItems.includes(elementName);
    }
  }

  onRemoveSelectedElement(elementName){
    //this.addedItems = this.addedItems.filter(el => el !== elementName);
    let indexOfElement = this.addedItems.indexOf(elementName)
    if (indexOfElement >= 0) {
      this.addedItems.splice(indexOfElement, 1);
    }
    let elementBackToTheList = this.allowedChildren.find(el=> el.Name === elementName);
    if (elementBackToTheList.Cardinality !== (SedaCardinalityConstants.zeroOrMore || SedaCardinalityConstants.oreOrMore)) {
      this.allowedChildrenNames.push(elementName);
    }
    let orderedNames = Object.values(this.allowedChildren.map(e=>e.Name));
    this.allowedChildrenNames.sort((a, b) => {
      return orderedNames.indexOf(a) - orderedNames.indexOf(b)
    })
    this.addedItems.length > 0 ? this.atLeastOneIsSelected = true : this.atLeastOneIsSelected = false
    this.upateButtonStatusAndDataToSend();
  }

  onAddSelectedElement(elementName){
    let sedaNode = this.sedaService.findSedaChildByName(elementName, this.sedaNodeFound);
      this.addedItems.push(elementName);
      if (sedaNode.Cardinality !== (SedaCardinalityConstants.zeroOrMore || SedaCardinalityConstants.oreOrMore)) {
        this.allowedChildrenNames = this.allowedChildrenNames.filter(name=> {return name !== elementName});
      }
      this.addedItems.length > 0 ? this.atLeastOneIsSelected = true : this.atLeastOneIsSelected = false
    this.upateButtonStatusAndDataToSend();
  }

  upateButtonStatusAndDataToSend(){
    this.popUpService.setPopUpDataOnClose(this.addedItems);
    this.popUpService.disableYesButton(!this.atLeastOneIsSelected)
  }

  onAllItemsAdded(){
    return this.allowedChildren.length === this.addedItems.length;
  }

  isElementComplex(elementName){
    let childFound = this.allowedChildren.find(el=> el.Name === elementName);
    if (childFound){
      return childFound.Element === SedaElementConstants.complex;
    }
  }

  onYesClick(): void {
    console.log("Clicked ok on dialog : %o" , this.selectedSedaNode);

  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Pipe({name: 'filterByName'})
export class FilterByNamePipe implements PipeTransform {
  transform(listOfNames: string[], nameToFilter: string): string[] {
    if(!listOfNames) return null;
    if(!nameToFilter) return listOfNames;

    return listOfNames.filter(n => n.toLowerCase().indexOf(nameToFilter) >= 0);
  }
}
