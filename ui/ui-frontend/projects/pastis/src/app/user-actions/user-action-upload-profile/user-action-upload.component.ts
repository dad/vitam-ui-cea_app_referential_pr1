import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { FileService } from '../../core/services/file.service';
import { ProfileService } from '../../core/services/profile.service';
import { FileTreeMetadataService } from 'src/app/file-tree/file-tree-metadata/file-tree-metadata.service';

const URL = 'http://localhost:8080/rest/createprofilefromfile';

@Component({
  selector: 'pastis-user-action-upload',
  templateUrl: './user-action-upload.component.html',
  styleUrls: ['./user-action-upload.component.scss']
})
export class UserActionUploadProfileComponent implements OnInit, OnDestroy {

  @Input()
  uploader: FileUploader = new FileUploader({url: URL});
  fileToUpload: File = null;

  //@Output()
  //updatedData: EventEmitter<any> =   new EventEmitter();

  constructor(private profileService: ProfileService,
    private fileMetadataService:FileTreeMetadataService,private fileService: FileService) { }

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload[0]);
  }

  uploadAndReload(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const formData = new FormData();
      formData.append('file', file, file.name);
      // Call upload profile service
      this.profileService.uploadProfile(formData).subscribe( fileData => {
        console.log('File submited! : ', fileData);
        // Emmmit JSON response from the uploaded data
        //this.updatedData.emit(fileData);
        //console.log('Data emmited: ', fileData);
        //Call file service and update the tree with the new data
        this.fileService.updateFileTree(fileData);
        //this.fileMetadataService.retrieveNodeMetadata(fileData[0]);

      });
    }
  }

  ngOnDestroy(): void {
  }



}
