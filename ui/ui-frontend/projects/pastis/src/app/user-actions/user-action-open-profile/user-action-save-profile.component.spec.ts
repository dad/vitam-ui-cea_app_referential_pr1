import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActionSaveProfileComponent } from './user-action-save-profile.component';

describe('UserActionOpenProfileComponent', () => {
  let component: UserActionSaveProfileComponent;
  let fixture: ComponentFixture<UserActionSaveProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActionSaveProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActionSaveProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
