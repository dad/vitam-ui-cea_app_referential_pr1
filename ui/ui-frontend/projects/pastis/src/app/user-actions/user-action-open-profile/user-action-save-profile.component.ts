import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/core/services/profile.service';
import { FileService } from 'src/app/core/services/file.service';
import { FileNode } from 'src/app/file-tree/classes/file-node';

@Component({
  selector: 'pastis-user-action-save-profile',
  templateUrl: './user-action-save-profile.component.html',
  styleUrls: ['./user-action-save-profile.component.scss']
})
export class UserActionSaveProfileComponent implements OnInit {

  data: FileNode[] = [];

  constructor(private profileService: ProfileService, private fileService: FileService) { }

  ngOnInit() {
  }


  postJsonToRNG() {
    //Retrieve the current file tree data as a JSON
    this.data = this.fileService.allData.getValue();

    console.log("On export button postJsonToRNG with current data %o",this.fileService.nodeChange.getValue());
      if (this.data) {
        // Send the retrieved JSON data to profile service
        this.profileService.updateFile(this.data).subscribe(retrievedData => {
          console.log("Data profile service: " + retrievedData)
          console.log('New updated data: ',  this.data);
          console.log('Data: ', retrievedData);
          this.downloadFile(retrievedData);
        });
      }
    }



  downloadFile(json): void {
    const newBlob = new Blob([json], { type: 'application/xml' });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
    }
    const data = window.URL.createObjectURL(newBlob);
    const link = document.createElement('a');
    link.href = data;
    link.download = 'pastis_profile.rng';
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
    setTimeout( () => {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
    }, 100);
}

}
