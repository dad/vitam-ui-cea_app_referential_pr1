import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActionRemoveMetadataComponent } from './user-action-remove-metadata.component';

describe('UserActionRemoveMetadataComponent', () => {
  let component: UserActionRemoveMetadataComponent;
  let fixture: ComponentFixture<UserActionRemoveMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActionRemoveMetadataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActionRemoveMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
