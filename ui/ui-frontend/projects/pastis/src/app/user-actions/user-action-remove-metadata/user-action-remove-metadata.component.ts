import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PastisDialogConfirmComponent } from 'src/app/shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { FileService } from 'src/app/core/services/file.service';
import { SedaService } from 'src/app/core/services/seda.service';
import { PopupService } from 'src/app/core/services/popup.service';
import { PastisDialogData } from 'src/app/shared/pastis-dialog/classes/pastis-dialog-data';

@Component({
  selector: 'pastis-user-action-remove-metadata',
  templateUrl: './user-action-remove-metadata.component.html',
  styleUrls: ['./user-action-remove-metadata.component.scss']
})
export class UserActionRemoveMetadataComponent implements OnInit {

  dataToSend:string;

  constructor(public dialogRef: MatDialogRef<PastisDialogConfirmComponent>,
    private fileService:FileService, private sedaService:SedaService,
    private popUpService: PopupService) { }

  ngOnInit() {
    setTimeout(() => {
      this.popUpService.setPopUpDataOnClose(this.dialogRef.componentInstance.dialogReceivedData.fileNode.name);
    },100);
  }
}
