import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, Input, OnDestroy, OnInit, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material';
import { BehaviorSubject, throwError } from 'rxjs';
import { SedaData, SedaElementConstants, SedaCardinalityConstants, SedaCollections } from './classes/seda-data';
import { SedaService } from '../core/services/seda.service';
import { NotificationService } from '../core/services/notification.service';
import { FileService } from '../core/services/file.service';
import { CardinalityConstants, FileNode, TypeConstants } from './classes/file-node';
import { FileTreeMetadataService } from './file-tree-metadata/file-tree-metadata.service';
import { UserActionAddMetadataComponent } from '../user-actions/user-action-add-metadata/user-action-add-metadata.component';
import { PastisDialogData } from '../shared/pastis-dialog/classes/pastis-dialog-data';
import { UserActionRemoveMetadataComponent } from '../user-actions/user-action-remove-metadata/user-action-remove-metadata.component';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { areAllEquivalent } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'pastis-file-tree',
  templateUrl: './file-tree.component.html',
  styleUrls: ['./file-tree.component.scss']
})
export class FileTreeComponent {

  @ViewChild('treeSelector', { static: true }) tree: any;
  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;


  @Input()
  nestedTreeControl: NestedTreeControl<FileNode>;
  @Input()
  nestedDataSource: MatTreeNestedDataSource<FileNode>;
  @Input()
  rootElementName: string;
  @Input()
  rootElementShowName: string;
  @Input()
  childrenListToExclude: string[];
  @Input()
  childrenListToInclude: string[];
  @Input()
  shouldLoad: boolean;
  @Input()
  collectionName:string

  nodeToSend: FileNode;

  //@Input()
  data: FileNode;

  newNodeName: string;

  sedaData: SedaData;

  selectedNode: SedaData;

  treeData: FileNode[];

  parentNodeMap = new Map<FileNode, FileNode>();

  dataChange = new BehaviorSubject<FileNode>(null);

  rulesChange: string[][] = [];

  static archiveUnits: FileNode;

  static archiveUnitsNumber: number;

  static uaIdAndPosition = new Map<string, number>();


  curentRootTabName: string;

  constructor(private fileService: FileService, private loggingService: NotificationService,
    private fileMetadataService: FileTreeMetadataService,
    private sedaService: SedaService,private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    if (this.shouldLoad) {
      this.sedaService.getSedaRules().subscribe(data => {
        this.sedaData = data[0];
        this.selectedNode = data[0];
        this.sedaService.selectedSedaNodeParent.next(this.sedaData);
        console.log("Init seda node on file tree : %o", this.selectedNode, " on tab : ", this.rootElementName);
      })
      this.fileService.addNode.subscribe(node => {
        this.addNewItem(node)
      })
      this.fileService.removeNode.subscribe(node => {
        this.remove(node)
      })
      this.fileService.tabChildrenRulesChange.subscribe(rules => {
        this.rulesChange = rules;
      })
      this.fileService.rootTabName.subscribe(rootNodeName=>{
        this.curentRootTabName = rootNodeName;
      })

    }
  }


  isAttribute = (node: FileNode): boolean => node.type === TypeConstants[TypeConstants.attribute];

  getChildren = (node: FileNode) => node.children;

  hasNestedChild(num: number, nodeData: FileNode):boolean {
    return !nodeData.type;
  }

  /** Select the category so we can insert the new item. */
  async addNewItem(node: FileNode) {
    let dataToSendToPopUp = <PastisDialogData>{};
    dataToSendToPopUp.titleDialog = "Veuillez sélectionner une ou plusieurs métadonnées"
    dataToSendToPopUp.subTitleDialog = `Ajouter des métadonnées à "${node.name}"`, node.name;
    dataToSendToPopUp.fileNode = node;
    dataToSendToPopUp.width = '800px';
    dataToSendToPopUp.okLabel = "Ajouter les métadonnées";
    dataToSendToPopUp.cancelLabel = "Annuler";
    dataToSendToPopUp.sedaNode = this.selectedNode;
    dataToSendToPopUp.component = UserActionAddMetadataComponent;
    dataToSendToPopUp.disableBtnOuiOnInit = true;
    const elementsToAdd = <string[]>await this.fileService.openPopup(dataToSendToPopUp);
    if (elementsToAdd) {
      //this.sedaService.selectedSedaNode.next(sedaNode);
      this.insertItem(node, elementsToAdd);
      elementsToAdd.length > 1 ?
      this.loggingService.showSuccess('Les métadonnées ' + elementsToAdd.join(', ') + ' ont été ajoutées') :
      this.loggingService.showSuccess('La métadonnée ' + elementsToAdd + ' a été ajoutée')
    }
  }

  /** Add an item (or a list of items) in the Tree */
  insertItem(parent: FileNode, elementsToAdd: string[]) {
    if (parent.children && elementsToAdd) {
      let sedaTabRootParent = this.sedaService.selectedSedaNodeParent.getValue();

      for (let element of elementsToAdd) {
        let newNode = {} as FileNode;
        let newId  = window.crypto.getRandomValues(new Uint32Array(10))[0];
        let sedaChild = this.sedaService.getSedaNodeLocally(sedaTabRootParent, element);

        newNode.name = element;
        newNode.id = newId;
        newNode.level = parent.level + 1;
        // Check if the type of node to be added is an attribute or not
        sedaChild.Element === SedaElementConstants.attribute ?
        newNode.type = TypeConstants.attribute :
        newNode.type = TypeConstants.element;

        newNode.parentId = parent.id;
        newNode.children = [];
        newNode.cardinality = Object.values(CardinalityConstants).find(c => c.valueOf() === sedaChild.Cardinality);
        console.log("Parent node name: " + parent.name);
        console.log("New node  : ", newNode);
        //Update parent->children relashionship
        parent.children.push(newNode);
        this.parentNodeMap.set(newNode, parent);
        console.log("Seda children and file children: ", this.selectedNode.Children, parent.children)
        // Insert all children of complex elements based on SEDA definition
        if (sedaChild.Element === SedaElementConstants.complex ) {
          let childrenOfComplexElement : string[] = [];
          sedaChild.Children.forEach(child=> {
            if (child.Cardinality === SedaCardinalityConstants.one ||
                child.Cardinality === SedaCardinalityConstants.oreOrMore) {
                  childrenOfComplexElement.push(child.Name);
            }
          })
          this.insertItem(newNode,childrenOfComplexElement);
        }
      }
        let sedaChildrenName: string[] = [];
        sedaTabRootParent.Children.forEach(child => {
          sedaChildrenName.push(child.Name);
        })
        //Order elements according to seda definition
        parent.children.sort((a, b) => {
            return sedaChildrenName.indexOf(a.name) - sedaChildrenName.indexOf(b.name)
        })
        console.log("After filter : ", parent.children)

        this.sendNodeMetadata(parent);
        console.log("New fileNode data is : %o", this.nestedDataSource.data)

    } else {
      console.log('No More Nodes can be inserted : No node was selected or node name is invalid');
    }
  }

  sendNodeMetadata(node: FileNode):void {
    this.updateFileTree(node);
    this.updateMedataTable(node);
    if (node.name === 'DataObjectGroup') {
      this.renderChanges(node,'DataObjectPackage');
    }
    if (node.name  === 'DescriptiveMetadata') {
      FileTreeComponent.archiveUnits = node;
      this.generateArchiveUnitsNumbers(node)
      console.log("Archive units : ", FileTreeComponent.archiveUnits)
    }
    else {
     this.renderChanges(node);
    }
  }

  generateArchiveUnitsNumbers(archiveUnit:FileNode):void {
      if ( archiveUnit.name === 'DescriptiveMetadata') {
        FileTreeComponent.uaIdAndPosition[archiveUnit.level - 1] = archiveUnit.id;
      }
      let counter = 0;
      archiveUnit.children.forEach(child=> {
        if (child.name === 'ArchiveUnit'){
          counter ++;
          FileTreeComponent.uaIdAndPosition[archiveUnit.level - 1 + "."  + counter] = child.id;
        }
      })
  }


    // Refresh Tree by opening an given node (option)
  // If the a node name is not prodived, the function will open the root tab element
  renderChanges(node:FileNode, nodeNameToExpand?:string) {
    let data : FileNode;
    if (nodeNameToExpand) {
      data = this.fileService.getFileNodeByName(this.fileService.allData.getValue()[0],nodeNameToExpand);
    } else{
      data = this.fileService.getFileNodeByName(this.fileService.allData.getValue()[0],this.curentRootTabName);
    }
    if (data) {
      let dataArray = [];
      dataArray.push(data);
      this.nestedDataSource.data = null;
      this.nestedDataSource.data = dataArray;
      this.nestedTreeControl.expand(node);
    }
  }

  updateMedataTable(node:FileNode){
    //let isNodeComplex = this.sedaService.checkSedaElementType(node.name,this.sedaService.selectedSedaNodeParent.getValue())
    let rulesFromService = this.fileService.tabChildrenRulesChange.getValue()
    let tabChildrenToInclude = rulesFromService[0];
    let tabChildrenToExclude = rulesFromService[1];
    let sedaParent = this.sedaService.selectedSedaNodeParent.getValue()
    let theNode = this.sedaService.getSedaNodeLocally(sedaParent, node.name);
    if(theNode.Element === SedaElementConstants.complex){
      this.fileService.nodeChange.next(node);
      if (node.children.length > 0) {
        this.fileMetadataService.shouldLoadMetadataTable.next(true);
        console.log("The the current tab root node is : ", node)
        this.sedaService.selectedSedaNode.next(theNode);
        let dataTable = this.fileMetadataService.fillDataTable(theNode, node, tabChildrenToInclude, tabChildrenToExclude);
        console.log("Filled data on table : ", dataTable, "...should load : ", this.fileMetadataService.shouldLoadMetadataTable.getValue());
        this.fileMetadataService.dataSource.next(dataTable);
      } else {
        this.fileMetadataService.shouldLoadMetadataTable.next(false);
        //console.log("Element complex has no children : ", this.fileMetadataService.shouldLoadMetadataTable.getValue())
      }
    }
  }

  // Updates the nested tab root tree and the data tree
  updateFileTree(node:FileNode){
    this.nestedDataSource.data[0] = node
    let allData = this.fileService.allData.getValue()[0]
    this.updateItem(node,allData);
    this.selectedNode = this.sedaService.selectedSedaNode.getValue();
  }

  sendNodeMetadataIfChildren(node: FileNode) {
    if (node.children.length) {
      this.sendNodeMetadata(node);
    }
  }

  isElementComplexAndHasChildren(node:FileNode){
      return node.children.some(child => child.type === TypeConstants.element);
  }


  onResolveName(node:FileNode) {
  /*     let archiveUnitNode = this.fileService.getFileNodeById(FileTreeComponent.archiveUnits,node.id) as FileNode;
    if (node.name === 'ArchiveUnit' && archiveUnitNode) {
      let nodeLevel = (FileTreeComponent.archiveUnits.children.indexOf(node) + 1);
      let parent = this.fileService.getFileNodeById(FileTreeComponent.archiveUnits,node.parentId)
      let parentLevel = parent.level - 2

      let archiveUnilNumber = nodeLevel > 0 ? nodeLevel : parentLevel
      let archiveUnitDecimal = parentLevel === 0 ? "" :  "." +parentLevel;
      return 'UA ' + archiveUnilNumber + archiveUnitDecimal ;
    } */
    return node.name;
  }

  async remove(node: FileNode) {
    let dataToSendToPopUp = <PastisDialogData>{};
    let nodeSedaInfo = this.sedaService.getSedaNodeLocally(this.selectedNode,node.name);
    let nodeType = nodeSedaInfo.Element == SedaElementConstants.attribute ? 'L\'attribut ' : 'La métadonnée '
    dataToSendToPopUp.titleDialog = "Voulez-vous supprimer " + nodeType + " \"" + node.name + "\" ?";
    dataToSendToPopUp.subTitleDialog = nodeSedaInfo.Element == SedaElementConstants.attribute ?
                                      "Suppression d'un attribut" : "Suppression d'une métadonnée";
    dataToSendToPopUp.fileNode = node;
    dataToSendToPopUp.sedaNode = this.selectedNode;
    dataToSendToPopUp.component =UserActionRemoveMetadataComponent;

    let popUpAnswer =  <FileNode> await this.fileService.openPopup(dataToSendToPopUp);
    if (popUpAnswer) {
      let deleteTypeText = nodeSedaInfo.Element == SedaElementConstants.attribute ? ' supprimé' : ' supprimée'
      this.removeItem(node, this.fileService.nodeChange.getValue());
      this.loggingService.showSuccess(nodeType + node.name + ' a été '  + deleteTypeText + ' avec succès');
    }
  }

  isSedaNodeObligatory(nodeName: string): boolean {
    if (this.sedaData) {
      for (let child of this.sedaData.Children) {
        if (child.Name === nodeName) {
          return child.Cardinality !== '1' ? true : false;
        }
      }
    }
  }

  buildFileTree(obj: object, level: number): FileNode[] {
    // This should recive Root node of Tree of Type FileNode
    // so we dont have to create a new node and use it as it is
    return Object.keys(obj).reduce<FileNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = {} as FileNode;
      node.id = level;
      node.level = level;
      node.name = key;
      node.parentId = null;
      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.type = value;
        }
      }
      return accumulator.concat(node);
    }, []);
  }

  /** Remove an item Tree node given a parent node and the child to be removed */
  removeItem(childToBeRemoved: FileNode, parentRootNode: FileNode) {
    const parentNode = this.findParent(childToBeRemoved.parentId, parentRootNode);
    console.log("On removeItem with node : ", childToBeRemoved, "and parent : ", parentNode);
    const index = parentNode.children.indexOf(childToBeRemoved);
    if (index !== -1) {
      parentNode.children.splice(index, 1);
      this.parentNodeMap.delete(childToBeRemoved);
      this.dataChange.next(this.data);
    }
    console.log("Deleted node : ", childToBeRemoved, "and his parent : ", parentNode);
    // Refresh the metadata tree and the metadatatable
    this.sendNodeMetadata(parentNode);
  }

  /** Update an item Tree node */
  updateItem(newRootNode: FileNode, allData:FileNode) {
    for (let idx in allData.children) {
      if (allData.children[idx].id === newRootNode.id) {
        allData.children[idx] = newRootNode;
        console.log("Node updated to : ", allData.children[idx])
      } else {
        this.updateItem(newRootNode,allData.children[idx]);
      }
    }
  }


  /** Find a parent tree node */
  findParent(id: number, node: FileNode): FileNode {
    if (node && node.id === id) {
      return node;
    } else {
      if (node.children && id) {
        for (let element = 0; node.children.length; element++) {
          if (element && node.children && node.children.length > 0 && node.children[element].children.length > 0) {
            return this.findParent(id, node.children[element]);
          } else {
            continue;
          }
        }
      }
    }
  }

  findParentLevel(nodeToFind:FileNode): number {
    let parentNodeToSearch = this.nestedDataSource.data;
    for (let node of parentNodeToSearch) {
      // For nested elements
      if (this.rootElementName === node.name && this.rootElementName === nodeToFind.name &&
        parentNodeToSearch[0].name === node.name && parentNodeToSearch[0].id !== nodeToFind.id) {
        return 1;
      }
      return nodeToFind.level - node.level;
    }
  }

  // Checks if a node belongs to the clicked tab collection.
  // For a given node, searches the required node in the seda.json file and
  // returns true if the node's value of "Collection" is equal to the clicked tab
  isPartOfCollection(node:FileNode):boolean {
    let sedaParent = this.sedaService.selectedSedaNodeParent.getValue();
      if (sedaParent) {
        let sedaChild = this.sedaService.getSedaNodeCollection(sedaParent,node.name,this.collectionName);
        return sedaChild ? true : false;
      }
  }

  testFunc(node:FileNode){
    let sedaParent : SedaData;
    let sedaChild : SedaData;
    if (this.collectionName === 'Objets') {
      sedaParent = this.sedaService.getSedaNodeLocally(this.sedaService.selectedSedaNode.getValue(),'DataObjectGroup')
      if (sedaParent){
        sedaChild = this.sedaService.getSedaNodeCollection(sedaParent,node.name,'Objets');
        return sedaParent.Collection + "..." + sedaChild.Collection
      } else {
        return "not found"
      }
    } else {
      sedaParent = this.sedaService.selectedSedaNode.getValue();
      sedaChild = this.sedaService.getSedaNodeCollection(this.sedaService.selectedSedaNodeParent.getValue(),node.name,this.collectionName);
      return sedaChild.Collection + "..." + sedaParent.Collection
    }

  }


  // Returns the positioning, in pixels, of a given node
  calculateNodePosition(node:FileNode):string{
    //Root node name
    if (node.name === this.rootElementName ){
      return new Number(28).toString();
    }

    //Root children with children
    if (node.children.length && node.name !== this.rootElementName) {
      return (new Number((this.findParentLevel(node) * 40) -16)).toString();
    }
    //Root children without children-
    if ((!node.children.length && node.name !== this.rootElementName)){
      return (new Number((this.findParentLevel(node) * 40) - 13)).toString();
    }
  }


  /** Error handler */
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  showAllowedChidren(node: FileNode) {
    if (this.childrenListToExclude) {
      return !this.childrenListToExclude.includes(node.name);
    }
  }

}
