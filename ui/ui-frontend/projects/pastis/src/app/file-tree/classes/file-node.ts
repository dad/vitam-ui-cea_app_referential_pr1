import { v4 as uuid } from 'uuid';
import { v4 } from 'uuid/interfaces';

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum TypeConstants {
  element = 'element',
  attribute = 'attribute'
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum CardinalityConstants {
  'Zero or More' = '0-N',
  'One Or More' = '1-N',
  'Optional' = '0-1',
  'Obligatoire' = '1',
}


/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum DataTypeConstants {
    string = 'string',
    dateTime = 'dateTime',
    date = 'date',
    ID = 'ID',
    'id' = 'id',
    anyURI = 'anyURI',
    token = 'token',
    tokenType = 'tokenType',
    undefined = 'undefined'
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum ValueOrDataConstants {
    value = 'Value',
    data = 'Data',
    nsName = 'nsName',
    undefined = 'Undefined'
}

export interface FileNode {
  id: number;
  parentId: number;
  name: string;
  valueOrData: ValueOrDataConstants;
  value: string;
  type: TypeConstants;
  dataType: DataTypeConstants;
  cardinality: string;
  level: number;
  documentation: string;
  children: FileNode[];
}
