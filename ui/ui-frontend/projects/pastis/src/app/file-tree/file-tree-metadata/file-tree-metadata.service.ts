import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CardinalityValues, MetadataHeaders } from 'src/app/core/classes/models';
import { CardinalityConstants, FileNode, TypeConstants } from '../classes/file-node';
import { SedaData } from '../classes/seda-data';
import { v4 as uuid } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class FileTreeMetadataService {

  cardinalityValues: CardinalityValues[] = [];
  allowedCardinality: Map<string, string[]>;
  dataSource = new BehaviorSubject<MetadataHeaders[]>(null);
  selectedCardinalities = new BehaviorSubject<string[]>([]);
  allowedSedaCardinalities = new BehaviorSubject<string[][]>([]);

  shouldLoadMetadataTable = new BehaviorSubject<boolean>(true);

  constructor() {
    this.initCardinalityValues();
  }

  initCardinalityValues() {
    for (let key in CardinalityConstants) {
      let cardinality: CardinalityValues = { value: CardinalityConstants[key], viewValue: key };
      this.cardinalityValues.push(cardinality);
    }
    this.allowedCardinality = new Map<string, string[]>();
    this.allowedCardinality.set('1', ['1']);
    this.allowedCardinality.set('', ['1']);
    this.allowedCardinality.set(null, ['1']);
    this.allowedCardinality.set(undefined, ['1']);
    this.allowedCardinality.set("null", ['1']);
    this.allowedCardinality.set('0-1', ['0-1', '1']);
    this.allowedCardinality.set('0-N', ['0-1', '0-N', '1-N', '1']);
    this.allowedCardinality.set('1-N', ['1', '1-N']);
  }

  fillDataTable(sedaChild: SedaData, clickedNode: FileNode, childrenToInclude:string[], childrenToExclude:string[]): MetadataHeaders[] {
    let data:MetadataHeaders[] = [];
    let allowedCardList: string[][];
    if (clickedNode.children.length > 0) {
      for (let child of clickedNode.children) {
       // There are cases where there are no childrenToExclude declared
       // So we must check if it exists to avoid and undefined of includes error
       if (childrenToExclude && !childrenToExclude.includes(child.name) && child.type !== TypeConstants.attribute) {
        data.push({
          id: child.id,
          nomDuChamp: child.name,
          valeurFixe: child.value,
          cardinalite: this.findSedaAllowedCardinalityList(sedaChild, child),
          commentaire: child.documentation,
          type: child.dataType,
          enumeration: this.getEnumerationFromSedaNodeChildren(sedaChild, child.name),
        })
       } else if (!childrenToExclude && child.type !== TypeConstants.attribute){
          data.push({
            id: child.id,
            nomDuChamp: child.name,
            valeurFixe: child.value,
            cardinalite: this.findSedaAllowedCardinalityList(sedaChild, child),
            commentaire: child.documentation,
            type: child.dataType,
            enumeration: this.getEnumerationFromSedaNodeChildren(sedaChild, child.name)})
        }
      }
    } // No children : it should be a SEDA simple elementa type.
     //  It this case, the retrieved data is the current clicked node data
      else {
        data.push({
          id: clickedNode.id,
          nomDuChamp: clickedNode.name,
          valeurFixe: clickedNode.value,
          cardinalite: this.findSedaAllowedCardinalityList(sedaChild, clickedNode),
          commentaire: clickedNode.documentation,
          type: clickedNode.dataType,
          enumeration: this.getEnumerationFromSedaNodeChildren(sedaChild, clickedNode.name),
        })
      }
    this.allowedSedaCardinalities.next(allowedCardList);
    this.selectedCardinalities.next(this.findCardinalities(clickedNode, sedaChild, data));
    console.log("Data on fillDataTable", data, "with selected cards :", this.selectedCardinalities.getValue());
    return data;
  }

  findSedaAllowedCardinalityList(sedaNode: SedaData, fileNode: FileNode): string[] {
    let allowedCardinalityListResult: string[] = [];
    let resultList: string[][] = [];

    // If the clicked node has the same name was the seda node, the node is already found
    if (sedaNode.Name === fileNode.name) {
      allowedCardinalityListResult = this.allowedCardinality.get(sedaNode.Cardinality);
      return allowedCardinalityListResult;
    }
    if (sedaNode.Children.length > 0) {
      // Search the sedaNode children to find the correnpondent cardinality list
      for (let child of sedaNode.Children) {
        if ((child.Name === fileNode.name) || (sedaNode.Name === fileNode.name)) {
          // Used in the case we wish to "correct" the node's cardinality, since
          // the seda cardinality wont include the cardinality retrieved by node's rng file.
          // In this case, the condition will return the rng file cardinality list
          // instead of node's cardinality list in accordance with the SEDA specification.
          //if (child.Cardinality !== sedaNode.Cardinality){
          //allowedCardinalityListResult = this.allowedCardinality.get(clickedNode.cardinality);
          //return allowedCardinalityListResult;
          //}
          allowedCardinalityListResult = this.allowedCardinality.get(child.Cardinality);
          resultList.push(allowedCardinalityListResult)
          this.allowedSedaCardinalities.next(resultList)

          return allowedCardinalityListResult;
        }
      }
    } else {
      //console.error("Final CARDINALITY LIST (NO seda children found) : ", allowedCardinalityListResult, " for ", sedaNode.Name);
      for (const [card, cardlist] of this.allowedCardinality) {
        if (card === fileNode.cardinality) {
          !fileNode.cardinality ? allowedCardinalityListResult.push("1") : allowedCardinalityListResult = cardlist;
          //result = cardlist;
          resultList.push(cardlist)
          this.allowedSedaCardinalities.next(resultList)
          //console.error("Final CARDINALITY LIST : ", allowedCardinalityListResult)
          return allowedCardinalityListResult;
        }
      }
    }
    this.allowedSedaCardinalities.next(resultList)

    if (allowedCardinalityListResult.length < 1) {
      //console.error("Card not found for : ", clickedNode.name, "..assuming attribute cardinality :", clickedNode.cardinality);
      allowedCardinalityListResult = this.allowedCardinality.get(fileNode.cardinality);
      //!clickedNode.cardinality ? result.push("1") : result = this.allowedCardinality[clickedNode.cardinality];
      return allowedCardinalityListResult;

    }
  }

  findCardinalities(clickedNode: FileNode, sedaNode: SedaData, data:MetadataHeaders[]):string[] {
    let childrenCardMap = new Map();
    let idsToKeep = data.map(name=>name.id);
    //console.error("On findCardinalities with data : ", idsToKeep, data)

    if (sedaNode.Children.length > 0) {
      for (let sedaChild of sedaNode.Children) {
        for (let fileNodechild of clickedNode.children) {
          if (fileNodechild.name === sedaChild.Name && idsToKeep.includes(fileNodechild.id)) {
            //console.error("Child on findCardinalities", fileNodechild.name, fileNodechild.cardinality, sedaChild.Cardinality, !fileNodechild.cardinality)
            !fileNodechild.cardinality ? childrenCardMap.set(fileNodechild.id,"1") : childrenCardMap.set(fileNodechild.id,fileNodechild.cardinality);
            //console.error("Cards list values so far....", childrenCard, "..last element : ", fileNodechild.name);
          }
        }
      }
    } else {
      //console.error("No clicked node children found for %s. Card must be : ", clickedNode.name, clickedNode.cardinality)
      !clickedNode.cardinality ? childrenCardMap.set(clickedNode.id,"1") : childrenCardMap.set(clickedNode.id,clickedNode.cardinality);
    }
    if (childrenCardMap.size < 1) {
      !clickedNode.cardinality ? childrenCardMap.set(clickedNode.id,"1") : childrenCardMap.set(clickedNode.id,clickedNode.cardinality);
    }
    return Array.from(childrenCardMap.values());
  }

  /**
   * Find the children of sedaParent and return the 'Enumeration' property
   * @param sedaParent the seda parent of the node we want to find
   * @param childName the name of the seda node we want to find
   */
  getEnumerationFromSedaNodeChildren(sedaParent: SedaData, childName: string): string[] {
    let sedaNode: SedaData = sedaParent.Children.find(c => c.Name === childName);
    if (sedaNode){
      return sedaNode.Enumeration
    }
    return [];
  }
  shouldLoadTable(){
    return this.shouldLoadMetadataTable.getValue();
  }
}
