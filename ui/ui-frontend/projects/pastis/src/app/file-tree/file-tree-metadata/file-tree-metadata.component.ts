import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { FileService } from '../../core/services/file.service';
import { CardinalityValues, MetadataHeaders } from 'src/app/core/classes/models';
import { SedaService } from '../../core/services/seda.service';
import { CardinalityConstants, DataTypeConstants, FileNode, ValueOrDataConstants, TypeConstants } from '../classes/file-node';
import { SedaElementConstants, SedaData } from '../classes/seda-data';
import { FileTreeMetadataService } from './file-tree-metadata.service';
import { PastisDialogData } from 'src/app/shared/pastis-dialog/classes/pastis-dialog-data';
import { EditAttributesPopupComponent } from './edit-attributes/edit-attributes.component';
import { AttributeData } from './edit-attributes/models/edit-attribute-models';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { element } from 'protractor';
import { FileTreeComponent } from '../file-tree.component';


@Component({
  selector: 'pastis-file-tree-metadata',
  templateUrl: './file-tree-metadata.component.html',
  styleUrls: ['./file-tree-metadata.component.scss'],
  // Encapsulation has to be disabled in order for the
  // component style to apply to the select panel.
  encapsulation: ViewEncapsulation.None,
})

export class FileTreeMetadataComponent {

  valueOrData = Object.values(ValueOrDataConstants);
  dataType = Object.values(DataTypeConstants);
  cardinalityList: string[];
  cardinalityLabels = Object.values(CardinalityConstants)

  //Mat table
  matDataSource: MatTableDataSource<MetadataHeaders>;

  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;

  displayedColumns: string[] = ['nomDuChamp', 'valeurFixe', 'cardinalite', 'commentaire'];

  clickedNode: FileNode = {} as FileNode;

  sedaData: SedaData = {} as SedaData;

  // The seda node that has been opened from the left menu
  selectedSedaNode: SedaData;

  selectedCardinalities: string[];

  allowedSedaCardinalityList: string[][];

  cardinalityValues: CardinalityValues[] = [];

  regexPattern: string;

  patternType: string;

  rowIndex: number;

  hoveredElement:string;

  buttonIsClicked:boolean;

  config: {};

  metadatadaValueFormControl = new FormControl('', [Validators.required,Validators.pattern(this.regexPattern)]);

  valueForm = this.fb.group({
    valeurFixe: ['', [ Validators.pattern(this.regexPattern) ]],
  });

  constructor(private fileService: FileService, private fileMetadataService: FileTreeMetadataService,
    private sedaService: SedaService,private fb: FormBuilder, private ngixService: NgxUiLoaderService) {
        this.config = {
        locale: 'fr',
        showGoToCurrent: false,
        firstDayOfWeek: 'mo',
        format: 'YYYY-MM-DD'
      };
  }

  ngOnInit() {
    //Subscription to fileNode service
    this.fileService.getCurrentFileTree().subscribe(fileTree => {
      if (fileTree) {
          this.clickedNode = fileTree[0];
          this.fileService.allData.next(fileTree);
          // Subscription to sedaRules
          this.sedaService.getSedaRules().subscribe(data => {
            if (this.clickedNode) {
              let rulesFromService = this.fileService.tabChildrenRulesChange.getValue();
              let tabChildrenToInclude = rulesFromService[0];
              let tabChildrenToExclude = rulesFromService[1];
              this.sedaService.selectedSedaNode.next(data[0]);
              this.selectedSedaNode = data[0];
              this.fileService.nodeChange.next(this.clickedNode)
              let filteredData = this.fileService.filteredNode.getValue();
              // Initial data for metadata table based on rules defined by tabChildrenRulesChange
              if (filteredData) {
                let dataTable = this.fileMetadataService.fillDataTable(this.selectedSedaNode, filteredData,tabChildrenToInclude,tabChildrenToExclude);
                this.matDataSource = new MatTableDataSource<MetadataHeaders>(dataTable);
              }
          }
          })
      }
    })

    this.fileMetadataService.selectedCardinalities.subscribe(cards => {
      this.selectedCardinalities = cards;
    });

    // File node
    this.fileService.nodeChange.subscribe(node => {
      this.clickedNode = node;
    })

    // Get Current sedaNode
    this.sedaService.selectedSedaNode.subscribe(sedaNode => {
      this.selectedSedaNode = sedaNode;
    })


    this.fileMetadataService.dataSource.subscribe(data => {
      this.matDataSource = new MatTableDataSource<MetadataHeaders>(data);
    })
  }

  getMetadataInputPattern(type:string) {
    if (type === 'date') {
      this.regexPattern = '([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}';
      return this.regexPattern;
    }
    if (type === 'TextType' || type === null) {
      this.regexPattern = '^[a-zA-X0-9 ]*$';
      return this.regexPattern;
    }
  }

  getMetadataInputType(element: MetadataHeaders) {
      if (element.type === 'date') {
        return 'date';
      }
      if (element.enumeration.length > 0) {
        return 'enumeration';
      }
  }

  findCardinality(event) {

    if (!event) {
      return CardinalityConstants.Obligatoire;
    } else {
      return event;
    }

  }

  isSedaCardinalityConform(cardList: string[],card:string){
    return cardList.includes(card);
  }

  findCardinalityName(clickedNode: FileNode) {
    if (!clickedNode.cardinality) {
      return "1"
    } else {
      return this.cardinalityValues.find(c => c.value == clickedNode.cardinality).value
    }
  }

  setNodeChildrenCardinalities(name: string, value: string) {

    if (this.clickedNode.children) {
      for (let i = 0; i < this.clickedNode.children.length; i++) {
        console.log("Event : ", name, value, this.clickedNode.children[i].name, this.clickedNode.children[i].name === name)
        if (this.clickedNode.children[i].name === name) {
          this.clickedNode.children[i].cardinality = value;
          console.log("Changed %s to : ", this.clickedNode.children[i].name,
            this.clickedNode.children[i].cardinality)
        }
      }
    }
    console.log(this.clickedNode.children)
  }

  setNodeValue(metadata:MetadataHeaders , value:string){
    if (this.clickedNode.name === metadata.nomDuChamp && this.clickedNode.id === metadata.id) {
      this.clickedNode.value = value
    }
    else {
      for (let node of this.clickedNode.children) {
        if (node.name === metadata.nomDuChamp && node.id === metadata.id) {
          node.value = value;
        }
      }
    }
  }

  setDocumentation(nodeName:string,comment:string) {
    if (this.clickedNode.name === nodeName) {
      this.clickedNode.documentation = comment
    }
    else {
      for (let node of this.clickedNode.children) {
        if (node.name === nodeName) {
          node.documentation = comment;
        }
      }
    }
  }

  isElementComplex(elementName){
    let childFound = this.selectedSedaNode.Children.find(el=> el.Name === elementName);
    if (childFound){
      return childFound.Element === SedaElementConstants.complex;
    }
  }

  onAddNode() {
    console.log("The parent is : ", this.selectedSedaNode,this.clickedNode)
    this.fileService.addNode.emit(this.clickedNode)
  }

  async onEditAttributesClick(fileNodeName:string) {
    let popData = {} as PastisDialogData;
    if (fileNodeName) {
      popData.fileNode = this.fileService.findChild(fileNodeName,this.clickedNode);
      popData.sedaNode = this.sedaService.findSedaChildByName(fileNodeName,this.selectedSedaNode);
      popData.subTitleDialog = 'Edition des attributs de';
      popData.titleDialog = fileNodeName;
      popData.width = '1120px';
      popData.component = EditAttributesPopupComponent
      popData.okLabel = "Valider"
      popData.cancelLabel = "Annuler"
      let popUpAnswer =  <AttributeData[]> await this.fileService.openPopup(popData);
      console.log("The answer for edit attributte was ", popUpAnswer);
      if (popUpAnswer) {
        let attributeFileNodeListToAdd: FileNode[] = [];
        let attributeFileNodeListToRemove: FileNode[] = [];
        popUpAnswer.filter(a=> a.selected).forEach(attr=>{
          let fileNode = {} as FileNode;
          fileNode.cardinality = attr.selectedCardinality;
          fileNode.value = attr.valeurFixe;
          fileNode.documentation = attr.commentaire;
          fileNode.name = attr.nomDuChamp;
          fileNode.type = TypeConstants.attribute;
          fileNode.children = [];
          attributeFileNodeListToAdd.push(fileNode);
        });
        popUpAnswer.filter(a=> !a.selected).forEach(attr=>{
          let fileNode:FileNode = {} as FileNode;
          fileNode.name = attr.nomDuChamp;
          attributeFileNodeListToRemove.push(fileNode);
        })
        if (attributeFileNodeListToAdd){
          let attrsToAdd = attributeFileNodeListToAdd.map(e=>e.name);
          let attributeExists = popData.fileNode.children.some(child => attrsToAdd.includes(child.name))
          if (!attributeExists) {
            this.fileService.insertItem(popData.fileNode , attributeFileNodeListToAdd,popData.sedaNode);
          }
        }
        if (attributeFileNodeListToRemove){
          let attrsToRemove = attributeFileNodeListToRemove.map(e=>e.name);
          this.fileService.removeItem(attributeFileNodeListToRemove ,popData.fileNode);
        }
      }
    }
  }

  onDeleteNode(nodeName:string){
    const nodeToDelete = this.fileService.getFileNodeByName(this.fileService.nodeChange.getValue(),nodeName);
    this.fileService.removeNode.emit(nodeToDelete)
    console.error("Node to be deleted : ", nodeName,nodeToDelete);
  }

  onButtonClicked(elementName:string,event:MouseEvent){
    this.hoveredElement = elementName;
  }

  isButtonClicked(name: string, data: MetadataHeaders){
    if (data) {
      this.hoveredElement = name;
      this.buttonIsClicked = true;
      return data.nomDuChamp === this.hoveredElement;
    }
  }

  isRowHovered(elementName){
    return this.hoveredElement === elementName;
  }

  onMouseOver(row: MetadataHeaders) {
    this.buttonIsClicked = false;
    this.hoveredElement = row.nomDuChamp
  }

  onMouseLeave(row: MetadataHeaders) {
    if(!this.buttonIsClicked) {
      this.hoveredElement = '';
    }
  }

  checkElementType(elementName?:string){
    if (this.selectedSedaNode){
      let nameToSearch = elementName ? elementName : this.sedaService.selectedSedaNode.getValue().Name;
      let nodeElementType = this.sedaService.checkSedaElementType(nameToSearch, this.selectedSedaNode);
      return nodeElementType === SedaElementConstants.complex;
    }
  }

  shouldLoadMetadataTable(){
    return this.fileMetadataService.shouldLoadMetadataTable.getValue();
  }

  /**
   * Returns a boolean if a given node has one or more attributes
   * regarding its seda specification
   * @param nodeName The node's name to be tested
   */
  hasAttributes(nodeName:string): boolean {

    const node = this.sedaService.findSedaChildByName(nodeName, this.selectedSedaNode);

    if (node && node.Children.length > 0) {
      return (node.Children.find(c=>c.Element==SedaElementConstants.attribute)!==undefined);
    }
    return false;
  }

  isSedaObligatory(name: string): boolean {
    return this.sedaService.isSedaNodeObligatory(name,this.selectedSedaNode);
  }

  getSedaDefinition(elementName:string) {
    for (let node of this.selectedSedaNode.Children){
      if (node.Name === elementName) {
        return node.Definition
      }
    }
    return ""
  }

  ngAfterViewInit(): void {

  }
  ngOnDestroy(){
  }
}
