import { TestBed } from '@angular/core/testing';

import { EditAttributesService } from './edit-attributes.service';

describe('EditAttributesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditAttributesService = TestBed.get(EditAttributesService);
    expect(service).toBeTruthy();
  });
});
