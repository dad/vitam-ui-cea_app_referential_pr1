import { Component, Inject, OnInit, } from '@angular/core';
import { MatCheckboxChange, MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { FileService } from 'src/app/core/services/file.service';
import { SedaService } from 'src/app/core/services/seda.service';
import { DataTypeConstants, FileNode, TypeConstants, ValueOrDataConstants, CardinalityConstants } from '../../classes/file-node';
import { SedaData, SedaCardinalityConstants, SedaElementConstants } from '../../classes/seda-data';
import { AttributeData } from '../edit-attributes/models/edit-attribute-models';
import { FileTreeMetadataService } from '../file-tree-metadata.service';
import { PastisDialogData } from 'src/app/shared/pastis-dialog/classes/pastis-dialog-data';
import { PopupService } from 'src/app/core/services/popup.service';

@Component({
  selector: 'pastis-edit-attributes',
  templateUrl: './edit-attributes.component.html',
  styleUrls: ['./edit-attributes.component.scss']
})
export class EditAttributesPopupComponent implements OnInit {

  displayedColumns: string[] = ['nomDuChamp', 'valeurFixe', 'cardinalite', 'commentaire', 'selected'];

  attributeCardinalities: string[];

  elementSedaCardinality:string;

  selectedValue:string[];

  parentFileNode:FileNode;

  // The datasource used by the DataTable in the popup
  // It's data contains the list of Attributes to display
  matDataSource: MatTableDataSource<AttributeData>;


  constructor(
    public dialogRef: MatDialogRef<EditAttributesPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogReceivedData: PastisDialogData,
    private sedaService: SedaService,
    private fileService: FileService,
    private fileTreeMetadataService: FileTreeMetadataService,
    private popUpService : PopupService
  ) { }

  ngOnInit() {
    this.fileService.getCurrentFileTree().subscribe( fileTree => {
      if (fileTree) {
        this.parentFileNode = fileTree[0];
      }
    });
    this.matDataSource = this.getDataSource(this.dialogReceivedData.sedaNode, this.dialogReceivedData.fileNode);
    this.initAttributeCardinality();
    // Subscribe any datasource change to setPopUpDataOnClose
    this.popUpService.setPopUpDataOnClose(this.matDataSource.data);
  }

  initAttributeCardinality(){
    for(let index in this.matDataSource.data){
      if (!this.matDataSource.data[index].selectedCardinality) {
        let sedaCard = this.sedaService.findSedaChildByName(this.matDataSource.data[index].nomDuChamp,
          this.dialogReceivedData.sedaNode)
        this.matDataSource.data[index].selectedCardinality = sedaCard.Cardinality;
      }
    }
  }

  setElementComment(elementName:string, newComment: string) {
    for(let idx in this.matDataSource.data) {
      if (this.matDataSource.data[idx].nomDuChamp === elementName) {
        this.matDataSource.data[idx].commentaire = newComment;
      }
    }
    console.log("ParentFileNode : ", this.parentFileNode);
    for (let node of this.parentFileNode.children) {
      if (node.name === elementName) {
          node.documentation = newComment;
      }
    }
  }

  setElementCardinality(elementName:string, newCard: string) {
    for(let idx in this.matDataSource.data) {
      if (this.matDataSource.data[idx].nomDuChamp === elementName) {
        this.matDataSource.data[idx].selectedCardinality = newCard;
      }
    }
    for (let node of this.parentFileNode.children) {
      if (node.name === elementName) {
          node.cardinality = newCard;
      }
    }
  }

  setElementValue(elementName:string, newValue: string) {
    for(let idx in this.matDataSource.data) {
      if (this.matDataSource.data[idx].nomDuChamp === elementName) {
        this.matDataSource.data[idx].valeurFixe = newValue;
        //console.error( this.matDataSource.data[idx].valeurFixe,newValue);
      }
    }
    for (let node of this.parentFileNode.children) {
      if (node.name === elementName) {
          node.value = newValue;
      }
    }
  }

  /**
   * Function that computes the "checked" state of the "select all" checkbox
   * If all checkboxs are checked, then the "select all" checkbox is checked
   */
  isChecked(): boolean {
    return this.matDataSource.data.filter(a=>!a.selected).length==0;
  }

  /**
   * Function that checks/unchecks all attributes
   * @param change
   */
  toggleAllAttributes(change: MatCheckboxChange):void {
    let selectAll = change.checked;
    this.matDataSource.data.forEach(a=>a.selected = selectAll);
  }

    /**
   * Function that checks/unchecks the attribute
   * @param change
   */
  toggleAttribute(change: MatCheckboxChange,elementName:string):void {
    let element = this.matDataSource.data.find(a=> a.nomDuChamp === elementName);
    element.selected = change.checked
  }


  /**
   * Returns the modified FileNode from the popup
   *
   * It parses the datasource of the DataTable to collect the attributes
   * and add them to the modified FileNode
   */
  getFileNodeFromPopup():FileNode {
    // We get the original FileNode that was passed to the popup
    let fileNode: FileNode = this.dialogReceivedData.fileNode;

    this.fileService.deleteAllAttributes(fileNode);

    // Map all selected AttributeData to FileNode and add them as children of the fileNode
    this.matDataSource.data
      .filter(attributeData => attributeData.selected)
      .forEach(attributeData => {
        let attributeFileNode: FileNode = {} as FileNode;
        attributeFileNode.id = window.crypto.getRandomValues(new Uint32Array(10))[0];
        attributeFileNode.cardinality = attributeData.selectedCardinality;
        attributeFileNode.children = [];
        attributeFileNode.dataType = DataTypeConstants.string;
        attributeFileNode.documentation = attributeData.commentaire;
        attributeFileNode.level = fileNode.level + 1;
        attributeFileNode.name = attributeData.nomDuChamp;
        attributeFileNode.parentId = fileNode.id;
        attributeFileNode.type = TypeConstants.attribute;
        attributeFileNode.value = attributeData.valeurFixe;
        attributeFileNode.valueOrData = ValueOrDataConstants.value;
        // Add the attribute to the filenode
    });

    return fileNode;
  }

  /**
   * Get the datasource required to feed the datatable in the popup
   *
   * This datasource consists of a list of AttributeData
   *
   * @param sedaNode The seda definition of the node we want to edit
   * @param fileNode The node which we want to edit attributes
   */
  getDataSource(sedaNode: SedaData, fileNode: FileNode):MatTableDataSource<AttributeData> {
    let attributeDataList:AttributeData[] = [];
    // Loop on all the attributes available for the node in the seda definition
    // Maps all the attributes node to AttributesData object
    this.sedaService.getAttributes(sedaNode).forEach(sedaAttribute=>{

      let attributeData : AttributeData = {} as AttributeData;
      attributeData.nomDuChamp=sedaAttribute.Name;
      attributeData.type=sedaAttribute.Element;

      // Check if the attribute is already added to the current node
      let fileAttribute = <FileNode> this.fileService.getAttributeByName(fileNode, sedaAttribute.Name);

      if (fileAttribute){
        // If the attribute is present in the FileNode
        // We fill in the fields with the corresponding values
        attributeData.valeurFixe=fileAttribute.value;
        console.error("File attribute value : ", fileAttribute)
        attributeData.selected=true;
        attributeData.commentaire=fileAttribute.documentation;
        attributeData.cardinalities= this.fileTreeMetadataService.allowedCardinality.get(fileAttribute.cardinality);
        attributeData.selectedCardinality=fileAttribute.cardinality;
      } else {
        // If the attribute is not present
        // We fill in defaults values
        attributeData.valeurFixe="";
        attributeData.selected=false;
        attributeData.commentaire="";
        attributeData.cardinalities= this.fileTreeMetadataService.allowedCardinality.get(sedaAttribute.Cardinality);
        attributeData.selectedCardinality="";
      }
      attributeDataList.push(attributeData);
    });
    // Create and return the datasource with the attribute's data
    let result = new MatTableDataSource<AttributeData>(attributeDataList);
    return result;
  }


  getSedaDefinition(elementName:string) {
    if(this.dialogReceivedData.sedaNode){
      for (let node of this.dialogReceivedData.sedaNode.Children){
        if (node.Name === elementName) {
          return node.Definition
        }
      }
    }
    return ""
  }

}
