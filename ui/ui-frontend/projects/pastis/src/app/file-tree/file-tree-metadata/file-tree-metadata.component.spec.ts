import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileTreeMetadataComponent } from './file-tree-metadata.component';

describe('FileTreeMetadataComponent', () => {
  let component: FileTreeMetadataComponent;
  let fixture: ComponentFixture<FileTreeMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileTreeMetadataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileTreeMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
