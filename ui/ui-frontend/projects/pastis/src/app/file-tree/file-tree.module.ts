import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { SharedModule } from '../shared/shared.module';
import { UserActionsModule } from '../user-actions/user-actions.module';
import { DpDatePickerModule } from 'ng2-date-picker';

import { MatTableModule} from '@angular/material/table';


import {
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatOptionModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTreeModule,
  MatDialogModule,
  MatSortModule,
  MatTooltipModule,
  MatMenuModule,
  MatProgressBarModule,
  MatRippleModule,
  MAT_RIPPLE_GLOBAL_OPTIONS,
  RippleGlobalOptions

} from '@angular/material';


import { FileTreeMetadataComponent } from './file-tree-metadata/file-tree-metadata.component';
import { FileTreeComponent } from './file-tree.component';
import { FiletreeFilterPipe } from './pipes/filetree-filter.pipe';
import { EditAttributesPopupComponent } from './file-tree-metadata/edit-attributes/edit-attributes.component';
import { FileTreeTestComponent } from './file-tree-test/file-tree-test.component';
import { PastisDialogConfirmComponent } from '../shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { PastisUnderConstructionComponent } from '../shared/pastis-under-construction/pastis-under-construction.component';
const globalRippleConfig: RippleGlobalOptions = {
  disabled: true,
  animation: {
    enterDuration: 0,
    exitDuration: 0
  }
};
@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatSelectModule,
    MatOptionModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTreeModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatMenuModule,
    SharedModule,
    UserActionsModule,
    MatRippleModule,
    DpDatePickerModule
  ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatSelectModule,
    MatOptionModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTreeModule,
    MatMenuModule,
    FileTreeComponent,
    MatRippleModule,
    FileTreeMetadataComponent
  ],
  providers:[{provide: MAT_RIPPLE_GLOBAL_OPTIONS, useValue: {disabled: true}}],
  declarations: [FileTreeMetadataComponent, FileTreeComponent,FiletreeFilterPipe,
    EditAttributesPopupComponent, FileTreeTestComponent],
  entryComponents: [FileTreeComponent,PastisDialogConfirmComponent,PastisUnderConstructionComponent, EditAttributesPopupComponent]
})

export class FileTreeModule {

}
