import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileTreeTestComponent } from './file-tree-test.component';

describe('FileTreeTestComponent', () => {
  let component: FileTreeTestComponent;
  let fixture: ComponentFixture<FileTreeTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileTreeTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileTreeTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
