import { Component, OnInit, ViewChild } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Router } from '@angular/router';
import { ToggleSidenavService } from '../core/services/toggle-sidenav.service';
import { RegisterIconsService } from '../core/services/register-icons.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  @ViewChild('treeSelector', { static: true }) tree: any;
  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;

  opened: boolean;
  router: string;
  events: string[] = [];

  constructor(private _router: Router,private sideNavService : ToggleSidenavService,
    private iconService:RegisterIconsService) {

      this.sideNavService.isOpened.subscribe(status=>{
        this.opened = status;
      })
  }

  ngOnInit() {
    this.iconService.registerIcons()
  }

  openSideNav(){
    this.opened = true;
  }


  closeSideNav(){
    this.opened = false;
  }
}
