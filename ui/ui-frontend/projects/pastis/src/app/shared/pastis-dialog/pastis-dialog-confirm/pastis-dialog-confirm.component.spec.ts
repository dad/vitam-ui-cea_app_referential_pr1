import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastisDialogConfirmComponent } from './pastis-dialog-confirm.component';

describe('PastisDialogConfirmComponent', () => {
  let component: PastisDialogConfirmComponent;
  let fixture: ComponentFixture<PastisDialogConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastisDialogConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastisDialogConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
