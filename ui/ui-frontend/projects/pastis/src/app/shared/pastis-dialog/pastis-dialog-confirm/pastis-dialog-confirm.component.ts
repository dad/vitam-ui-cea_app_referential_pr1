import { Component, OnInit, Inject, Output, EventEmitter, Input, ViewChild, TemplateRef } from '@angular/core';
import { PastisDialogData } from '../classes/pastis-dialog-data';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SedaService } from '../../../core/services/seda.service';
import { ComponentPortal } from '@angular/cdk/portal';
import { PopupService } from 'src/app/core/services/popup.service';

@Component({
  selector: 'pastis-pastis-dialog-confirm',
  templateUrl: './pastis-dialog-confirm.component.html',
  styleUrls: ['./pastis-dialog-confirm.component.scss']
})
export class PastisDialogConfirmComponent implements OnInit {

  portal: ComponentPortal<any>;

  dataBeforeClose:any;

  btnYesShouldBeDisabled:boolean ;

  constructor(
    public dialogConfirmRef: MatDialogRef<PastisDialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogReceivedData: PastisDialogData,
    public sedaService: SedaService, private popUpService:PopupService) {
  }


  ngOnInit() {
    console.log("Data received on confirm dialog : %o", this.dialogReceivedData);
    if (this.dialogReceivedData.component){
      this.portal = new ComponentPortal(this.dialogReceivedData.component);
    }
    setTimeout(() => {
      if (!this.dialogReceivedData.okLabel) this.dialogReceivedData.okLabel = 'Oui'
      if (!this.dialogReceivedData.cancelLabel) this.dialogReceivedData.cancelLabel = 'Non'

      this.popUpService.popUpDataBeforeClose.subscribe(data=>{
        this.dataBeforeClose = data;
      })
      this.popUpService.btnYesShoudBeDisabled.subscribe(shouldDisableButton=>{
          this.btnYesShouldBeDisabled = shouldDisableButton;
      })
      this.popUpService.btnYesShoudBeDisabled.next(this.dialogReceivedData.disableBtnOuiOnInit)
    }, 50);

  }

  onNoClick(): void {
    console.log("Clicked no ");
    this.popUpService.btnYesShoudBeDisabled.next(false)
    this.dialogConfirmRef.close();
  }

  onYesClick(): void {
    console.log("Clicked ok on dialog and send data : %o", this.dataBeforeClose);
  }

  getToolTipData(data) {
    if (data && data.length) {
      return data.nodeName
    }
  }

  ngOnDestroy() {

  }


}
