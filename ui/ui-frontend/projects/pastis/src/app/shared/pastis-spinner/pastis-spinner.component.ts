import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'pastis-pastis-spinner',
  templateUrl: './pastis-spinner.component.html',
  styleUrls: ['./pastis-spinner.component.scss']
})

export class PastisSpinnerComponent implements OnDestroy{

  isLoading = new BehaviorSubject<boolean>(false);



  @Input()
  color: string = 'accent';
  mode = 'indeterminate';
  //@Input()
  value = 20;

  constructor(){ }

  ngOnDestroy(): void {
  }

}


