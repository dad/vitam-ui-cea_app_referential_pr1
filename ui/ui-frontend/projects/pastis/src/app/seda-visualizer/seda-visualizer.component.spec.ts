import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedaVisualizerComponent } from './seda-visualizer.component';

describe('SedaVisualizerComponent', () => {
  let component: SedaVisualizerComponent;
  let fixture: ComponentFixture<SedaVisualizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedaVisualizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedaVisualizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
