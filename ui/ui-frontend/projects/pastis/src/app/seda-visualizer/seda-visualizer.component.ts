import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SedaService } from '../core/services/seda.service';
import { SedaData } from '../file-tree/classes/seda-data';
import { PastisApiService } from '../core';
import { chdir } from 'process';
import { NgxUiLoaderService } from 'ngx-ui-loader';
let d3 = require('d3');


@Component({
  selector: 'pastis-seda-visualizer',
  templateUrl: './seda-visualizer.component.html',
  styleUrls: ['./seda-visualizer.component.scss']
})
export class SedaVisualizerComponent implements OnInit {

  @ViewChild('myDiv',{static:true}) myDiv: ElementRef;

  sedaData:SedaData;

  private getSedaUrl = './assets/seda_lower.json';

  constructor(private pastisService: PastisApiService, private loaderService:NgxUiLoaderService) {
  }

  ngOnInit() {
    this.pastisService.getLocally(this.getSedaUrl).subscribe(sedaRules=> {
      this.sedaData = sedaRules[0]

      var margin = {top: 20, right: 120, bottom: 20, left: 120},
      width = 1800 - margin.right - margin.left,
      height = 800 - margin.top - margin.bottom;

    var i = 0,duration = 550, root;

    var tree = d3.layout.tree()
      .size([height, width]);

    var diagonal = d3.svg.diagonal()
      .projection(function(d) { return [d.y, d.x]; });

    var svg = d3.select("div").append("svg")
      .attr("width", width + margin.right + margin.left)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    root = sedaRules[0];
    root.x0 = height / 2;
    root.y0 = 0;
    update(root);

    d3.select(self.frameElement).style("height", "500px");

    function update(source) {
      var legend = require('d3-svg-legend/no-extend')

      // Compute the new tree layout.
      var nodes = tree.nodes(root);
      var links = tree.links(nodes);


      // Normalize for fixed-depth.
      nodes.forEach(function(d) { d.y = d.depth * 230; });

      // Update the nodes…
      var node = svg.selectAll("g.node")
        .data(nodes, function(d) { return d.id || (d.id = ++i); })

      // Enter any new nodes at the parent's previous position.
      var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("text", "A")
        .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
        .style("cursor","pointer")
        .on("click", click);

        //Adda circle instead of a image
        nodeEnter.append("circle")
        .attr("r", 1e-6)
        .style("stroke", "#604379")
        .style("stroke-width", "2px")
        .style("fill", function(d) { return d.children ? "#604379" : "#fff"; });


         /* Add image instead of a circle
        nodeEnter.append("image")
        .attr("xlink:href", function(d) {
          if (d.Element === 'Simple') return "./assets/svg/icons/simple_icon.png";
          if (d.Element === 'Complex') return "./assets/svg/icons/complex_icon.png";
          if (d.Element === 'Attribute') return "./assets/svg/icons/attribute_icon.png"; })
        .attr("x", "-12px")
        .attr("y", "-12px")
        .attr("width", "24px")
        .attr("height", "24px")
        .attr("background-color", );
        */


        nodeEnter.append("text")
        .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
        .attr("dy", ".35em")
        .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
        .text(function(d) { return d.Name; })
        .style("fill-opacity", 1e-6)
        .style("font",'12px sans-serif');

        // Letters inside circle
        nodeEnter.append("text")
        .attr("x", function(d) { return d.children || d._children ? 4 : -4; })
        .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
        .attr("dy", ".35em")
        .attr("stroke","#65B2E4")
        .attr("stroke-width","1px")
        .text(function(d) {
          if (d.Element === 'Simple') return "S";
          if (d.Element === 'Complex') return "C";
          if (d.Element === 'Attribute') return "A"; })
        .style("fill-opacity", 1e-6)
        .style("font",'12px sans-serif');

      // Transition nodes to their new position.
      var nodeUpdate = node.transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

      nodeUpdate.select("circle")
        .attr("r", 12)
        .style("fill", function(d) { return d.children ? "#604379" : "#fff"; })

      nodeUpdate.select("text")
        .style("fill-opacity", 1);

      // Transition exiting nodes to the parent's new position.
      var nodeExit = node.exit().transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
        .remove();

      nodeExit.select("circle")
        .attr("r", 1e-6)

      nodeExit.select("text")
        .style("fill-opacity", 1e-6);

      // Update the links…
      var link = svg.selectAll("path.link")
        .data(links, function(d) { return d.target.id; });


      // Enter any new links at the parent's previous position.
      link.enter().insert("path", "g")
        .style("fill","none")
        .style("stroke","#ccc")
        .style("stroke-width","2px")
        .attr("class", "link")
        .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
        });

      // Transition links to their new position.
      link.transition()
        .duration(duration)
        .attr("d", diagonal);

      // Transition exiting nodes to the parent's new position.
      link.exit().transition()
        .duration(duration)
        .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
        })
        .remove();

      //Legend
      // select the svg area
      var svg_legend = d3.select("#my_dataviz")
      svg_legend.append("circle").attr("cx",20).attr("cy",30).attr("r", 6).attr("r", 12).style("stroke", "#604379").style("stroke-width", "2px").style("fill","#fff" )
      svg_legend.append("text").attr("x","15").attr("dy", "35").attr("stroke","#65B2E4").text("C").style("fill-opacity", 1e-6).style("font",'12px sans-serif');
      //Simple element circle and text
      svg_legend.append("circle").attr("cx",180).attr("cy",30).attr("r", 6).attr("r", 12).style("stroke", "#604379").style("stroke-width", "2px").style("fill","#fff" )
      svg_legend.append("text").attr("x","176").attr("dy", "35").attr("stroke","#65B2E4").text("S").style("fill-opacity", 1e-6).style("font",'12px sans-serif');
      //Attribute circle and text
      svg_legend.append("circle").attr("cx",330).attr("cy",30).attr("r", 6).attr("r", 12).style("stroke", "#604379").style("stroke-width", "2px").style("fill","#fff" )
      svg_legend.append("text").attr("x","326").attr("dy", "35").attr("stroke","#65B2E4").text("A").style("fill-opacity", 1e-6).style("font",'12px sans-serif');

      //Legend text
      svg_legend.append("text").attr("x", 40).attr("y", 30).text("Complex Element").style("font-size", "15px").attr("alignment-baseline","middle")
      svg_legend.append("text").attr("x", 200).attr("y", 30).text("Simple Element").style("font-size", "15px").attr("alignment-baseline","middle")
      svg_legend.append("text").attr("x", 350).attr("y", 30).text("Attribute").style("font-size", "15px").attr("alignment-baseline","middle")

      // Stash the old positions for transition.
      nodes.forEach(function(d) {
      d.x0 = d.x;
      d.y0 = d.y;
      });
    }

      // Toggle Children on click.
      function click(d) {
        if (d.children) {
          d._children = d.children;
          d.children = null;
        } else {
          d.children = d._children;
          d._children = null;
        }
        update(d);
      }

    })
  }


}
