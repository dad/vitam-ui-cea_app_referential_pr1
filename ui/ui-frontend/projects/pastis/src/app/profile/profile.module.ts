import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from './../core/core.module'
import { SharedModule } from './../shared/shared.module'
import { FileTreeModule } from './../file-tree/file-tree.module'


import {MatIconModule} from '@angular/material/icon'
import {MatTabsModule} from '@angular/material/tabs'


import {CreateProfileComponent} from '../profile/create-profile/create-profile.component'
import {EditProfileComponent} from '../profile/edit-profile/edit-profile.component'
import {ListProfileComponent} from '../profile/list-profile/list-profile.component'
import { ProfileComponent } from './profile.component';


@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    MatIconModule,
    MatTabsModule,
    SharedModule,
    FileTreeModule
  ],
  exports: [CreateProfileComponent,EditProfileComponent,ListProfileComponent],
  providers:[],
  declarations: [ProfileComponent,CreateProfileComponent,EditProfileComponent,ListProfileComponent],

})
export class ProfileModule { }
