import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { SedaVisualizerComponent } from './seda-visualizer/seda-visualizer.component';
import { PastisUnderConstructionComponent } from './shared/pastis-under-construction/pastis-under-construction.component';
import { HomeComponent } from './home/home.component';
import { AccountComponent, ActiveTenantGuard, AppGuard, AuthGuard } from 'ui-frontend-common';



const routes: Routes = [
  {
    // we use PORTAL_APP as our appId so that the AppGuard won't find a profile with this appId
    // and we'll be redirected to the Portal Application
    path: '',
    component: AppComponent,
    canActivate: [AuthGuard, AppGuard],
    data: { appId: 'PORTAL_APP' }
    },
  {
    path: 'pastis',
    component: HomeComponent,
    canActivate: [AuthGuard, AppGuard],
    data: { appId: 'PORTAL_APP' }
  },
  // =====================================================
  //                      PASTIS
  // =====================================================
  {
    path: 'sedaview',
    component: SedaVisualizerComponent
  },
  // =====================================================
  //                      unknown path
  // =====================================================
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    ActiveTenantGuard,
    AuthGuard
  ]
})
export class AppRoutingModule { }

export const routingComponents = [SedaVisualizerComponent,HomeComponent];
